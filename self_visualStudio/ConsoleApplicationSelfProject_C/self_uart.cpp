
#include <stdio.h>

typedef unsigned          	char uint8;
typedef	unsigned			short uint16;
typedef unsigned           	int uint32;
#define			int8			char
#define			int16			short
#define			int32			int

struct self_uart_stor
{
	void* rx_buffer;
	uint16 read_index, save_index, save_size;
};

/*uart's struct initialise*/
#define self_uart_stor_init {\
NULL,\
NULL,\
NULL,\
NULL,\
}

self_uart_stor self_uart_store_rx = self_uart_stor_init;

void self_uart_buffer_stack(void *buff, uint32 size)
{
	self_uart_store_rx.rx_buffer = buff;
	self_uart_store_rx.save_size = size;
}


/**@brief uart reciveing data
*
* @param[in] rx_data: data accepted
*/
void self_uart_save(uint8 rx_data)
{
	struct self_uart_stor *int_rx = &self_uart_store_rx;

	if (int_rx->rx_buffer == NULL)
		return;
	/* save character */
	*(((uint8 *)int_rx->rx_buffer)+int_rx->save_index) = rx_data&0xff;

	int_rx->save_index++;
	if (int_rx->save_index >= int_rx->save_size)
		int_rx->save_index = 0;

	/* if the next position is read index, discard this 'read char' */
	if (int_rx->save_index == int_rx->read_index)
	{
		int_rx->read_index++;
		if (int_rx->read_index >= int_rx->save_size)
			int_rx->read_index = 0;
	}
}

/**@brief read recived data
*
* @param[in] buffer: data accepted
			 size  : data sized
*/
int32 self_uart_read(void* buffer, uint32 size)
{
	uint8* ptr = (uint8*)buffer;
	struct self_uart_stor* int_rx = &self_uart_store_rx;

	if (int_rx->rx_buffer == NULL)
		return NULL;
	/* interrupt mode Rx */
	while (size)
	{
		if (int_rx->read_index != int_rx->save_index)
		{
			/* read a character */
			*ptr++ = *(((uint8*)int_rx->rx_buffer) + int_rx->read_index);
			size--;
			/* move to next position */
			int_rx->read_index++;
			if (int_rx->read_index >= int_rx->save_size)
				int_rx->read_index = 0;

		}
		else
		{
			/* set error code */
			break;
		}
	}
	return (uint32)ptr - (uint32)buffer;
}

/**@brief uart's data processing
*
* @param[in] 
*/
#define TRUE 1
#define FALSE 0
#define UART_READ_SIZE 100
uint16 uart_pro_post= 0;
uint16 uart_pro_len = 0;
uint8 rx_sav[UART_READ_SIZE];
void self_uart_data_process(void)
{
	uint16 rx_size, i;
	bool is_data_valid = FALSE;
	bool is_proto_ok = FALSE;
	struct self_uart_stor* int_rx = &self_uart_store_rx;
	if (NULL == int_rx->rx_buffer)
		return;

	uint8 rx_tmp[UART_READ_SIZE];
	rx_size = self_uart_read(rx_tmp, UART_READ_SIZE);
	if (rx_size)
	{
		for (i = 0; i < UART_READ_SIZE;i++)
		{
			switch (uart_pro_post)
			{
			case 1: // protocol header
				is_data_valid = TRUE;
				break;
			case 2:	//data length
				uart_pro_len = rx_tmp[uart_pro_post];
				break;
			default:
				if (uart_pro_len)
				{
					uart_pro_len--;
					if (0 == uart_pro_len)
					{
						is_proto_ok = TRUE;
					}
				}
				break;
			}
			if (TRUE == is_data_valid)
			{
				~is_data_valid;
				rx_sav[uart_pro_post++] = rx_tmp[i];

				if (is_proto_ok)
				{
					//manipulation data
				}
				if (uart_pro_post >= UART_READ_SIZE)
				{
					uart_pro_post = 0;
				}
			}
		}
	}
}

int main_uart(void)
{
	//self_uart_store_rx = self_uart_stor_init;
	return 1;
}