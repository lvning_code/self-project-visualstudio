/**@brief main
*/

#include "self_accsToHex.h"
extern uint8_t HexToAscs(uint8_t *hex, uint8_t * ascii);

void  hexToAscs(uint8_t hex, char *ascs)
{

	uint8_t h, l;
	h = (hex >> 4) & 0x0f;
	l = (hex & 0x0f);


	if (h <= 9)
		ascs[0] = h + 0x30;
	else if ((h >= 10) && (h <= 15)){
		ascs[0] = h + 0x41 - 10;
	}
	else{
		ascs[0] = 0xff;
	}

	if (l <= 9)
		ascs[1] = l + 0x30;
	else if ((l >= 10) && (l <= 15)){
		ascs[1] = l + 0x41 - 10;
	}
	else{
		ascs[1] = 0xff;
	}
}

uint16_t HexsToAscs(uint8_t *hexs, char * ascs, uint16_t length)
{
	uint16_t j = 0;
	for (uint16_t i = 0; i<length; i++){
		hexToAscs(hexs[i], ascs + j);
		j += 2;
	}
	return j;
}

void accs_data(uint8_t* _data2, uint8_t *_data, uint8_t *cmd_chn, uint8_t len)
{
	uint16_t i = 0, k = 0;
	uint8_t uanalyze_data[100] = { 0 };
	printf("\r\nlen:%d\r\n",len);
	for (i = 0; i < len; i++)
	{
		k = i % 15;
		uanalyze_data[i] = _data[i] ^ *(cmd_chn + k);
		if (uanalyze_data[i] >= 0x10)
			printf("0x%x ", uanalyze_data[i]);
		else
			printf("0x0%x ", uanalyze_data[i]);

	}
	printf("\t\r\n");
	for (i = 0; i < len; i++)
	{
		if (uanalyze_data[i] >= 0x10)
			printf("%c", uanalyze_data[i]);
		else
			printf("%c", uanalyze_data[i]);
	}
}

void self_accsTohex_main_text(void)
{
	uint8_t i = 0;
	uint8_t cmd_data2_buf[128] = { 0 };
	uint8_t cmd_data_buf[128] = { 0 };
	uint8_t chn_imei[15] = {0};

	char* CMD = "35060D0709282237206E710C01026609180806160176606B060D0908";
	char* chn = "868681044749149";

	//iemi
	uint8_t chnlen = strlen(chn);
	memcpy(chn_imei, chn, 15);
	chnlen = sizeof(chn_imei);
	printf("\r\nchnlen:%d,chn:\r\n",chnlen);
	for (i = 0; i < chnlen; i++)
	{
		uint8_t imei = *(chn_imei + i);
		if (imei >= 0x10)
			printf("0x%x ", imei);
		else
			printf("0x0%x ", imei);
	}

	//����
	uint8_t cmd_len = strlen(CMD);
	Init_AccsHex_Lib(2, cmd_len);
	printf("\r\ncmd_len:%d\r\n", cmd_len);
	ascsToHexs(CMD, cmd_data_buf, cmd_len);
	cmd_len /= 2;
	accs_data(cmd_data2_buf,cmd_data_buf, chn_imei, cmd_len);

	printf("\r\n\n");
}
extern void self_accsTohex_main_text(void);
void main(void)
{
	//self_accsTohex_main();
	self_accsTohex_main_text();

}