﻿// arrayPearAndPearArray.cpp : 定义控制台应用程序的入口点。
//
// Project1-end.cpp : 定义控制台应用程序的入口点。
//

#include <stdio.h>
int main()
{
	int arr[2][3] = { 1, 2, 3, 4, 5, 6 };
	printf("%p\n", (arr));//	00F8FAF0
	printf("%p\n", (arr + 1));//00F8FAFC
	printf("%d\n", sizeof(arr)); //	   24
	printf("%d\n", sizeof(arr + 1));// 4
	printf("%d\n", sizeof(int[3]));//  12
	//【arr 是数组地址，不可以赋值】 arr 是一个int [2][3]的类型，arr的操作范围是2,指向的是 int[3] 的类型
	//a+1 移动位置 a+sizeof(int[3])*1
	//数组不是指针，因为它带有限定范围
	//int **p = arr;  //p 是指向int* 类型的，p+1 操作是p+sizeof(int *)*1
	//arr+1的操作不想等
	int* ptr[1];
	ptr[0] = arr[0];
	//arr[0]是 int[]的类型，arr[0]的操作范围在3,最终指向的是int
	//ptr[0]是 int*的类型，ptr[0]是指针，指向的是int
	int **pp = NULL;
	int(*p)[3] = arr;
	//【p 是数组地址，是不可以赋值】p 是一个 int[3]* 的指针，p 指向的是int[3]类型的，
	//p+1的操作是：p+sizeof(int[3])*1;这与数组 arr操作是的相同；
	int *_p = (int *)(arr);
	//_p = arr[0]; //     与上操作相同，此时arr[0] 它指向int
	//此时（arr）是一个指针，指向 int型的数据，_p 也是一个指向int 的指针；
	//_p+1 操作：_p+sizeof(int)；
	printf("%d\n", *(_p + 1));// 2

	return 0;
}









