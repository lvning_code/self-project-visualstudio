
/**************************************************************************//**
 * @file	self_i2c_cfg.h 	
 * @brief For IIC communication
 * @author ning lv
 * @version 0.01
 ******************************************************************************
 * @section License
 ******************************************************************************
 *
 *****************************************************************************/

#ifndef _SELF_I2C_CFG_H_
#define _SELF_I2C_CFG_H_
#ifdef __cplusplus
	extern "C" {
#endif

#include <stdint.h>
#include "self_i2c.h"  //master and the explanation is here!!

//#include "self_printf.h"

/*
*** Define variables and structures ... */
//typedef   unsigned          char uint8_t;
typedef   unsigned			char uint8;
typedef   unsigned           int uint32;
#define NOP()				( __nop())
/*  You can choose yours projects ...*/
//#define NRF51822   //试用在蓝牙芯片上
//#define STM32F205	   //
#define SELF_WINDOWS


/*!<\  sda 、scl*/
typedef void(*pI2cLineF)(uint32 sda, uint32 scl);

/*!<\  gpio   */
typedef void(*pI2cGpioF)(uint32 gpo);

/* Chip typedf ... */
#if defined NRF51822
#define CHOOSE_IN     				_I2C_TRUE_
#define CHOOSE_OUT    				_I2C_NULL_
#elif defined STM32F205
#include "stm32f2xx.h"
#define CHOOSE_IN     				(GPIO_Mode_IN )
#define CHOOSE_OUT    				(GPIO_Mode_OUT)
#define GPIO_PORT           		((GPIO_TypeDef*)self_I2Cstr.GPIO)
#else
#define CHOOSE_IN     				_I2C_TRUE_
#define CHOOSE_OUT    				_I2C_NULL_
#endif

#define I2C_LOG_DEBUG 4
#if   I2C_LOG_DEBUG==1
#define I2c_Log_Debug(x,y...) printf(x,##y)
#elif I2C_LOG_DEBUG==2
#ifdef SELF_PRINTF_H
#define I2c_Log_Debug self_printf
#endif
#elif I2C_LOG_DEBUG==3
#define I2c_Log_Debug(x,y...)
#elif I2C_LOG_DEBUG==4
#define I2c_Log_Debug printf
#endif


/* Function declaration to use ...*/
uint8 self_gpio_pin_read(uint32 pin);
void pin_gpio_type(uint32 type);
void pin_i2c_linenum(uint32 sda, uint32 scl);
void self_pin_I2c_dir(uint32 pin, uint8 dir);
void self_gpio_pin_write(uint32 pin, uint8 value);

/* I2C 的读写引用 */
void self_i2c_stop(void);
void self_i2c_noack(void);
void self_i2c_recvAck(void);
void self_i2c_reset(void);
uint8 self_i2c_start(void);
uint8 self_i2c_waitack(void);
uint8 self_i2c_recvByte(void);
void self_i2c_sendByte(uint8 c);
void self_pin_i2c_cfg(uint32 sda, uint32 scl, uint32 type);//设置 sda 和 scl 的引脚设置.

/* I2C 的例子 */
void self_delayms(uint32 volatile number_of_ms);
void self_delayus(uint32 volatile number_of_us);//时间必须调试确定#######################
uint8_t DEVICE_I2C_ReadByte(uint8_t Dev_Addr, uint8_t Reg_Addr, uint8_t *c, uint8_t len);//写
uint8_t DEVICE_I2C_WriteByte(uint8_t Dev_Addr, uint8_t Reg_Addr, uint8_t *c, uint8_t len);//读

#ifdef __cplusplus
	}
#endif
#endif /*_SELF_I2C_CFG_H_*/

