
#include "self_i2c_cfg.h"  //master and the explanation is here!!

/*
***The global variable ... */
self_I2c_PinStr self_I2Cstr;
pI2cGpioF _I2cGpio = pin_gpio_type;
pI2cLineF _I2cLine = pin_i2c_linenum;

/**@brief Delay 1us
 */
 void Delay1us(void)
{
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
#if defined STM32F205	
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
	NOP();NOP();NOP();NOP();
#endif
}
/**@brief Delay 5us
 */
void Delay5us(void)
{
    Delay1us();
	Delay1us();
	Delay1us();
	Delay1us();
}

/**@brief Delay 10us
 */
void Delay10us(void)
{
	Delay5us();
	Delay5us();
}

/**@brief Delay(ms)  
 *
 * @param[in] number_of_ms 
 */
void self_delayms(uint32 volatile number_of_ms)
{
	number_of_ms = number_of_ms * 1000;
    while(number_of_ms != 0)
    {
		number_of_ms--;
		 Delay1us();
    }
}

/**@brief Delay(us)  
 *
 * @param[in] number_of_us 
 */
void self_delayus(uint32 volatile number_of_us)
{
	 while(number_of_us != 0)
    {
        number_of_us--;
		Delay1us();
			
    }
}

/**@brief Iic_Pin_Dir  ֧���ļ� STM32F2xx or NRF51822 
 *
 * @param[in] dir 
 */
#if defined ( STM32F205 )
void STM32F205_Pin_Dir (uint32 pin, uint8 dir)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	if(GPIO_Mode_OUT == dir)
	{
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	}
	else if(GPIO_Mode_IN == dir)
	{
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	}
	GPIO_InitStructure.GPIO_Pin =  pin;
	GPIO_Init(GPIO_PORT, &GPIO_InitStructure);
}
#elif defined ( NRF51822 )
void nRF51822_Pin_Dir (uint32 pin, uint8 dir)
{
	if ( dir == 0 ) 
	{
		nrf_gpio_cfg_output(pin);
		
	}else 
	{
		nrf_gpio_cfg_input(pin,NRF_GPIO_PIN_PULLUP);
	}       
}
#else defined (SELF_TEST_I2C)
#endif


/**@brief Select the self_iic sda and scl pins. 
 */
void pin_i2c_linenum(  uint32 sda,uint32 scl  )
{
	self_I2Cstr.SDA = sda;
	self_I2Cstr.SCL = scl;
}

/**@brief Function Pointers set GPIO
 */
void pin_gpio_type( uint32 type )
{
	self_I2Cstr.GPIO = type;
}

/**@brief Function Pointers set the sda and SCL pins
 */
void self_pin_i2c_cfg( uint32 sda,uint32 scl, uint32 type)
{
#if defined STM32F205
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
#endif
	_I2cGpio(type);
	_I2cLine(sda,scl);
}

/**@brief The pin line outputs 0/1.
 *
 * @param[in] dir 
 */
void self_pin_I2c_dir (uint32 pin, uint8 dir)
{
#if defined ( NRF51822 )
		nRF51822_Pin_Dir (pin, dir);
#elif defined ( STM32F205 )
		STM32F205_Pin_Dir (pin, dir);
#else

#endif   
}


/**@brief SDA/SCL line write 0/1.
 *
 * @param[in] pin ,wr
 */

void self_gpio_pin_write(uint32 pin ,uint8 value)
{
	if ( value == 0 ) {
#if defined ( NRF51822 )
	 nrf_gpio_pin_write(pin,0);
#elif defined ( STM32F205 )
	 GPIO_WriteBit(GPIO_PORT , pin , 0);
#endif 
	}
	else {
#if defined ( NRF51822 )
		 nrf_gpio_pin_write(pin,1);
#elif defined ( STM32F205 )
		 GPIO_WriteBit(GPIO_PORT , pin , 1);
#endif 
	}       
}

/**@brief SDA/SCL line read 0/1.
 *
 * @param[in] pin 
 */
uint8 self_gpio_pin_read(uint32 pin)
{
#if defined ( NRF51822 )
	 return ((uint8) nrf_gpio_pin_read(pin));
#elif defined ( STM32F205 ) 
	return (uint8) GPIO_ReadInputDataBit(GPIO_PORT , pin);
#else
	return 0;
#endif 	
}	





