/**************************************************************************//**
 * @file	self_i2c.c
 * @brief For IIC communication
 * @author ning lv
 * @version 0.02
 ******************************************************************************
 * @section License
 ******************************************************************************
 *
 *****************************************************************************/
 
//Header files ...
#include "self_i2c.h"

/**@brief reset
*/
void self_i2c_reset(void)
{
	if (RD_SDA_IN == 0
		/*|| READ_SCL_IN == 0*/)
	{
		SCL_SET_OUT;
		SDA_SET_OUT;
		self_i2c_nop(I2C_DELAY_TM);

		SDA_OUT_H;
		SCL_OUT_H;
		self_i2c_nop(I2C_DELAY_TM);

		self_i2c_sendByte(0xFF);
		self_i2c_noack();
		self_i2c_stop();
	}
}

/**@brief IIC communication to prepare.
 *
 * @param[in] i2c_master 
 */
uint8_t self_i2c_start(void)
{
	/* 当SCL高电平时，SDA出现一个下跳沿表示总线启动信号 */
	self_i2c_reset();
	
  	SCL_SET_OUT;
  	SDA_SET_OUT;  
	
  	SDA_OUT_H;
  	SCL_OUT_H;
  	self_i2c_nop(I2C_DELAY_TM);
  	if( !RD_SDA_IN ) {return I2C_BUSY;}			/*!< BUSY */
	SDA_OUT_L;	
  	self_i2c_nop(I2C_DELAY_TM);
  	if( RD_SDA_IN )  {return I2C_ERROR;}		/*!< ERROR */
  	SDA_OUT_L;															/*!< CPU占线 */
  	return I2C_I2C_TRUE_;
}

/**@brief IIC stop the communication .
 *
 * @param[in] i2c_master 
 */
void self_i2c_stop(void)
{
	/* 当SCL高电平时，SDA出现一个上跳沿表示总线停止信号 */
	SCL_OUT_L;
  	self_i2c_nop(I2C_DELAY_TM);
  	SDA_OUT_L;
  	self_i2c_nop(I2C_DELAY_TM);
  	SCL_OUT_H;
  	self_i2c_nop(I2C_DELAY_TM);
  	SDA_OUT_H;
}


/**@brief Send the data 'c' out,either as data or as an address.
 *
 * @param[in] C 
 */
void self_i2c_sendByte(uint8_t value)
{
   	uint8 i = 8;
  
	while(i--) 
	{
		SCL_OUT_L;		/*允许SDA 数据跳变*/
		self_i2c_nop(I2C_DELAY_TM); 
		if( value & 0x80) 
		{
			SDA_OUT_H;	
		}
		else 
		{
			SDA_OUT_L;
		}				
			
		value<<=1;  		
		SCL_OUT_H;  	/*SDA 数据保持 1us*/
		self_i2c_nop(I2C_DELAY_TM);
	}	
	SCL_OUT_L;	
	SDA_OUT_H;		 /*CPU 释放总线*/
}

/**@brief Reads a bytes of data and returns it.
 *
 * @param[out] retc 
 */
uint8 self_i2c_recvByte(void)
{
   	uint8 i = 8;
   	uint8 retc = 0;
	
   	SDA_SET_IN;
   	SDA_OUT_L;/*总线默认拉高*/
   	while(i--) 
	{
		retc<<=1;
		SCL_OUT_L;  /*CPU 允许 SDA 数据跳变*/
		self_i2c_nop(I2C_DELAY_TM);
		SCL_OUT_H;  /*CPU 保持 SDA 数据1us*/
		self_i2c_nop(I2C_DELAY_TM);
		if( RD_SDA_IN ) 
		{
	   		retc |= 0x01;
		}
   	}
   	SCL_OUT_L;
   	SDA_SET_OUT;
   	SDA_OUT_H; /*CPU 释放总线*/
		
   	return retc;	
}
/**@brief watint ACK 
 */
uint8 self_i2c_waitack(void)
{
	uint8 errTime = 250; 
	uint8 retFlag = I2C_I2C_TRUE_;
	
	SCL_OUT_L;
	SDA_OUT_H; 								/*CPU 释放总线*/
	self_i2c_nop(I2C_DELAY_TM);
	
	SCL_OUT_H; 								/*CPU驱动SCL = 1, 此时器件会返回ACK应答*/
	SDA_SET_IN;
  	self_i2c_nop(I2C_DELAY_TM);
	while((RD_SDA_IN)&&((errTime--) > 0))
	{
		if(errTime == 0)
		{
			retFlag = I2C_FALSE;
		}
	}
	SCL_OUT_L;
	SDA_SET_OUT;
	return retFlag;
}
/**@brief ACK response
 */
void self_i2c_recvAck(void)
{
	SCL_OUT_L;	/*CPU 允许 SDA 数据跳变*/
	self_i2c_nop(I2C_DELAY_TM);	
	SDA_OUT_L;  /*CPU 拉低SDA ,ACK器件*/
	self_i2c_nop(I2C_DELAY_TM);
	SCL_OUT_H;	/*CPU 保持 SDA 数据1us*/
	self_i2c_nop(I2C_DELAY_TM);
	SCL_OUT_L;
	self_i2c_nop(I2C_DELAY_TM);
	SDA_OUT_H;	/* CPU释放SDA总线 */
}

/**@brief Don't reply 
 */
void self_i2c_noack(void)
{
  	SCL_OUT_L;
  	self_i2c_nop(I2C_DELAY_TM);	
  	SDA_OUT_H;
  	self_i2c_nop(I2C_DELAY_TM);
  	SCL_OUT_H;
  	self_i2c_nop(I2C_DELAY_TM);
  	SCL_OUT_L;
  	self_i2c_nop(I2C_DELAY_TM);
}


