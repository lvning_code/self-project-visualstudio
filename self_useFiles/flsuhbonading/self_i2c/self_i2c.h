
#ifndef _SELF_IIC_H_
#define _SELF_IIC_H_
#ifdef __cplusplus
	extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include "self_i2c_cfg.h"  //master and the explanation is here!!

/* Define some of the things ...*/
#define _I2C_NULL_		0
#define _I2C_TRUE_		1
#define I2C_ERROR	    _I2C_NULL_     //returns 
#define I2C_FALSE		_I2C_NULL_
#define I2C_I2C_TRUE_	_I2C_TRUE_     
#define I2C_BUSY		3    
#define I2C_DELAY_TM	5  		//slot time (4.7us)

/*!<\ Redefines the exact width signed integer type ...*/
typedef unsigned          	char uint8;
typedef unsigned           	int uint32;

/*!<\ Define variables and structures ... */
typedef struct {
	uint32 SDA;
	uint32 SCL;
	uint32 GPIO;
}self_I2c_PinStr;

extern self_I2c_PinStr self_I2Cstr;

/*Output 0/1 ... */
#define SDA_OUT_H	               	( self_gpio_pin_write(self_I2Cstr.SDA,_I2C_TRUE_) )
#define SDA_OUT_L	               	( self_gpio_pin_write(self_I2Cstr.SDA,_I2C_NULL_) )
#define SCL_OUT_H	               	( self_gpio_pin_write(self_I2Cstr.SCL,_I2C_TRUE_) )
#define SCL_OUT_L	               	( self_gpio_pin_write(self_I2Cstr.SCL,_I2C_NULL_) )

/*Choose output/input ... */
#define SDA_SET_IN	                ( self_pin_I2c_dir (self_I2Cstr.SDA,CHOOSE_IN) ) 
#define SDA_SET_OUT	                ( self_pin_I2c_dir (self_I2Cstr.SDA,CHOOSE_OUT))
#define SCL_SET_IN_	                ( self_pin_I2c_dir (self_I2Cstr.SCL,CHOOSE_IN) )
#define SCL_SET_OUT	                ( self_pin_I2c_dir (self_I2Cstr.SCL,CHOOSE_OUT))

/* Reads lines ... */
#define RD_SDA_IN	                ( self_gpio_pin_read(self_I2Cstr.SDA) ) 
#define RD_SCL_IN	                ( self_gpio_pin_read(self_I2Cstr.SCL) )

/* times ... */
#define self_i2c_nop(x)				( self_delayus(x) )

#ifdef __cplusplus
	}
#endif
#endif /*SELF_IIC_H_*/
