
#include "self_i2c_cfg.h"  //master and the explanation is here!!
//#include "self_usart.h"

#define TYPE_ADDR _I2C_TRUE_
#define TYPE_REG  _I2C_NULL_
#define SUCCEED   _I2C_TRUE_
#define FAIL	  _I2C_NULL_
#define READ	  _I2C_TRUE_

/*
***varibles...*/
static uint8_t i2c_busy = _I2C_NULL_;
//...static function
/**@brief IIC start 
 *
 * @param[in] self_i2c_start()
 * @param[out] SUCCEED , FAIL
 */
static uint8_t DEVICE_I2c_Start(const uint8_t res)
{
	switch(res)
		{
			case I2C_BUSY: I2c_Log_Debug("...I2C_BUSY  \r\n");return FAIL; //return Fail;
			case I2C_ERROR: I2c_Log_Debug("...I2C_ERROR  \r\n");return FAIL; //return Fail;
			default: return SUCCEED;
		}
}
/**@brief send address and printf logs  
 *
 * @param[in]  device address or registor address ,type = TYPE_ADDR or type = TYPE_REG
 */
static uint8_t DEVICE_I2c_SendAddr(const uint8_t Addr, const uint8_t type)
{
	self_i2c_sendByte(Addr);//Device address
	if(!self_i2c_waitack())
	{
		if(type)
		{
			I2c_Log_Debug("...Address no reply :%02x  !\r\n",Addr);
		}
		else
		{
			I2c_Log_Debug("...Register address error :%02x  !\r\n",Addr);
		}
		return FAIL;
	}	
	return SUCCEED;
}

/*
*** example IIC ...*/
/**@brief write IIC  
 *
 * @param[in]  device address, registor address ,data is *c ,data's lengths
 */
uint8_t DEVICE_I2C_WriteByte (uint8_t Dev_Addr, uint8_t Reg_Addr, uint8_t *c,uint8_t len )
{
    uint8_t i = _I2C_NULL_;
	i2c_busy = _I2C_TRUE_;

	if(!DEVICE_I2c_Start(self_i2c_start())){
		goto DEVICE_WriteByte_FAIL; //return Fail;
	}
	if(!DEVICE_I2c_SendAddr(Dev_Addr,TYPE_ADDR)){
		goto DEVICE_WriteByte_FAIL; //return Fail;
	}
	if(!DEVICE_I2c_SendAddr(Reg_Addr,TYPE_REG)){
		goto DEVICE_WriteByte_FAIL; //return Fail;
	}
		
	for(i=_I2C_NULL_;i<len;i++){
		self_i2c_sendByte(*c++);	
		if(!self_i2c_waitack()){
			I2c_Log_Debug("...Write data failed\r\n");
		}
	}
   	self_i2c_stop();
	i2c_busy = _I2C_NULL_;
   	return SUCCEED;	
	
DEVICE_WriteByte_FAIL:
	self_i2c_stop();
	i2c_busy = _I2C_NULL_;
   	return FAIL;	    
} 

/**@brief read IIC  
 *
 * @param[in]  device address, registor address ,data is *c ,data's lengths
 */
uint8_t DEVICE_I2C_ReadByte  (uint8_t Dev_Addr,uint8_t Reg_Addr,uint8_t *c,uint8_t len)
{
    uint8_t i = _I2C_NULL_;
	i2c_busy = _I2C_TRUE_;
	
	if(!DEVICE_I2c_Start(self_i2c_start())){
		goto DEVICE_II_ReadByte_FAIL; //return Fail;
	}
	
	if(!DEVICE_I2c_SendAddr(Dev_Addr,TYPE_ADDR)){
		goto DEVICE_II_ReadByte_FAIL; //return Fail;
	}
	
	if(!DEVICE_I2c_SendAddr(Reg_Addr,TYPE_REG)){
		goto DEVICE_II_ReadByte_FAIL; //return Fail;
	}
	if(!DEVICE_I2c_Start(self_i2c_start())){
		goto DEVICE_II_ReadByte_FAIL; //return Fail;
	}
		
	if(!DEVICE_I2c_SendAddr((Dev_Addr|READ),TYPE_ADDR)){
		goto DEVICE_II_ReadByte_FAIL; //return Fail;
	}
		
	for(i=_I2C_NULL_;i<len;i++){
			*c++ = self_i2c_recvByte();
	}
   	self_i2c_noack();
   	self_i2c_stop();
	i2c_busy = _I2C_NULL_;
   	return SUCCEED;
  
DEVICE_II_ReadByte_FAIL:
	self_i2c_stop();
	i2c_busy = _I2C_NULL_;
    return FAIL;
}



