#include <stdarg.h>
#include "self_printf.h"

const unsigned char vail[16]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

//int self_out_float(float m,unsigned char c,unsigned char w,unsigned char wd);

int self_outc(char c,unsigned char w)              //输出字符
{   
	unsigned char i;
  for(i=1;i<w;i++)
	 {
		PUTCHAR(' ');
   }
	PUTCHAR(c);
	return 0;
}

int self_outs(___va_list_ p)                                 //输出字符串
{ 
  PUTSTRING( p);
	return 0;
}


int self_out_num( int m,int base,unsigned char c,unsigned char w)  //输出数字
{   
   unsigned int n;
   unsigned char buf[maxparments],count=0,i;
	 unsigned char *s = (unsigned char *)buf+sizeof(buf);
	
	 *--s=' ';
	
	 if(m<0)
		 n=-m;
	 else 
		 n=m;
	 do
	 {
     *--s=vail[n%base];
		 n=n/base;
     count++;
	 }while(n!=0);
	
	 for(i=1;i<=(w-count);i++)
	 {
		*--s=c;
   }
	 if(m<0)
	 	*--s='-';
	 self_outs((___va_list_)s);
	 return 0;
}
int ___va_printf(const char* fmt,char* p)
{  
  while(*fmt!='\0')
   	{
		unsigned char width=0;      //总位宽
		unsigned char width_decimal=0;  //小数位宽
		unsigned char lead=' ';     //默认前导为空格
		if(*fmt=='%')          
    	{
			fmt++;
			if(*fmt=='0')
			{
				lead='0';
				fmt++;
			}
			while((*fmt>='0')&&(*fmt<='9'))    //得到总位宽
			{
				width=width*10;
				width=width+(*fmt)-'0';
				fmt++;
			}
			if(*fmt=='.')             //得到小数位宽
			{
				fmt++;
				while((*fmt>='0')&&(*fmt<='9'))
				{
					width_decimal=width_decimal*10;
					width_decimal=width_decimal+(*fmt)-'0';
					fmt++;
				}
			}
			switch(*fmt)              //调用相应的函数
			{
				case 'd': self_out_num(___va_arg(p,int),10,lead,width); break;
				case 'o': self_out_num(___va_arg(p,unsigned int),8,lead,width); break;
				case 'x': self_out_num(___va_arg(p,unsigned int),16,lead,width) ;break;
				case 'c': self_outc(___va_arg(p,int),width); break;
				case 's': self_outs(___va_arg(p,char *)); break;
				//case 'f': out_float(___va_arg(p,float),lead,width,width_decimal); break;
				default: self_outc(*fmt,1); break;
			}
		 }
		else
		{
			PUTCHAR(*fmt);
			fmt++;
		}
	 }
	 return 0; 
}

int self_printf(const char* format,...)
{
  ___va_list_ apt;
	___va_start(apt,format);
	___va_printf(format,apt);
	___va_end(apt);
  return 0;
}

 int self_printf_test(void) //测试函数
{
	self_printf("test char      =%c,%c\n\r", 'A','a') ;	
	self_printf("test decimal number =%d\n\r",  123456) ;
	self_printf("test decimal number =%d\n\r",  -123456) ;	
	self_printf("test hex   number =0x%x\n\r", 0x55aa55aa) ;	
	self_printf("num=%08d\n\r",  12345);
	self_printf("num=%8d\n\r",  12345);
	self_printf("num=0x%08x\n\r", 0x12345);
	self_printf("num=0x%8x\n\r", 0x12345);
	self_printf("num=0x%02x\n\r", 0x1);
	self_printf("num=0x%2x\n\r", 0x1);
	self_printf("num=%05d\n\r", 0x1);
	self_printf("num=%5d\n\r", 0x1);
	return 0;

}



 //调用<stdarg.h>基础，定义重写。
 static void t_printchar(char **str, int c)
 {
	 extern int self_putchar(char c);

	 if (str) {
		 **str = c;
		 ++(*str);
	 }
	 else (void)self_putchar(c);
 }

#define PAD_RIGHT 1
#define PAD_ZERO 2

 static int t_prints(char **out, const char *string, int width, int pad)
 {
	 register int pc = 0, padchar = ' ';

	 if (width > 0) {
		 register int len = 0;
		 register const char *ptr;
		 for (ptr = string; *ptr; ++ptr) ++len;
		 if (len >= width) width = 0;
		 else width -= len;
		 if (pad & PAD_ZERO) padchar = '0';
	 }
	 if (!(pad & PAD_RIGHT)) {
		 for (; width > 0; --width) {
			 t_printchar(out, padchar);
			 ++pc;
		 }
	 }
	 for (; *string; ++string) {
		 t_printchar(out, *string);
		 ++pc;
	 }
	 for (; width > 0; --width) {
		 t_printchar(out, padchar);
		 ++pc;
	 }

	 return pc;
 }

 /* the following should be enough for 32 bit int */
#define PRINT_BUF_LEN 12

 static int t_printi(char **out, int i, int b, int sg, int width, int pad, int letbase)
 {
	 char print_buf[PRINT_BUF_LEN];
	 register char *s;
	 register int t, neg = 0, pc = 0;
	 register unsigned int u = i;

	 if (i == 0) {
		 print_buf[0] = '0';
		 print_buf[1] = '\0';
		 return t_prints(out, print_buf, width, pad);
	 }

	 if (sg && b == 10 && i < 0) {
		 neg = 1;
		 u = -i;
	 }

	 s = print_buf + PRINT_BUF_LEN - 1;
	 *s = '\0';

	 while (u) {
		 t = u % b;
		 if (t >= 10)
			 t += letbase - '0' - 10;
		 *--s = t + '0';
		 u /= b;
	 }

	 if (neg) {
		 if (width && (pad & PAD_ZERO)) {
			 t_printchar(out, '-');
			 ++pc;
			 --width;
		 }
		 else {
			 *--s = '-';
		 }
	 }

	 return pc + t_prints(out, s, width, pad);
 }

 static int t_print(char **out, const char *format, va_list args)
 {
	 register int width, pad;
	 register int pc = 0;
	 char scr[2];

	 for (; *format != 0; ++format) {
		 if (*format == '%') {
			 ++format;
			 width = pad = 0;
			 if (*format == '\0') break;
			 if (*format == '%') goto out;
			 if (*format == '-') {
				 ++format;
				 pad = PAD_RIGHT;
			 }
			 while (*format == '0') {
				 ++format;
				 pad |= PAD_ZERO;
			 }
			 for (; *format >= '0' && *format <= '9'; ++format) {
				 width *= 10;
				 width += *format - '0';
			 }
			 if (*format == 's') {
				 register char *s = (char *)va_arg(args, int);
				 pc += t_prints(out, s ? s : "(null)", width, pad);
				 continue;
			 }
			 if (*format == 'd') {
				 pc += t_printi(out, va_arg(args, int), 10, 1, width, pad, 'a');
				 continue;
			 }
			 if (*format == 'x') {
				 pc += t_printi(out, va_arg(args, int), 16, 0, width, pad, 'a');
				 continue;
			 }
			 if (*format == 'X') {
				 pc += t_printi(out, va_arg(args, int), 16, 0, width, pad, 'A');
				 continue;
			 }
			 if (*format == 'u') {
				 pc += t_printi(out, va_arg(args, int), 10, 0, width, pad, 'a');
				 continue;
			 }
			 if (*format == 'c') {
				 /* char are converted to int then pushed on the stack */
				 scr[0] = (char)va_arg(args, int);
				 scr[1] = '\0';
				 pc += t_prints(out, scr, width, pad);
				 continue;
			 }
		 }
		 else {
		 out:
			 t_printchar(out, *format);
			 ++pc;
		 }
	 }
	 if (out) **out = '\0';
	 va_end(args);
	 return pc;
 }

 int t_printf(const char *format, ...)
 {
	 va_list args;

	 va_start(args, format);
	 return t_print(0, format, args);
 }

 int t_sprintf(char *out, const char *format, ...)
 {
	 va_list args;

	 va_start(args, format);
	 return t_print(&out, format, args);
 }


 int t_snprintf(char *buf, unsigned int count, const char *format, ...)
 {
	 va_list args;

	 (void)count;

	 va_start(args, format);
	 return t_print(&buf, format, args);
 }


