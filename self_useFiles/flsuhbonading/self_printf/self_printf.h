
#ifndef SELF_PRINTF_H
#define SELF_PRINTF_H


//#include "stdio.h"
typedef char * ___va_list_;

#define maxparments  255

#define _INTSIZEOF(n)  ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) ) //保证是4字节对齐

#define ___va_start(ap,v) ( ap = (___va_list_)&v + _INTSIZEOF(v) )
 /*如果定义了两个表达式，先计算表达式1，再计算表达式2，返回表达式2的值*/

#define ___va_arg(ap,t)  ( ap =ap+ _INTSIZEOF(t),*(t *)(ap- _INTSIZEOF(t))) 
#define ___va_end(ap)   ( ap = (___va_list_)0 )

#ifdef __stdio_h
#define PUTCHAR(x)  putchar(x)  
#define PUTSTRING(y) puts(y)
#else
/*\\notes: uses  */
#define PUTCHAR(x)   self_putchar((char)x)
#define PUTSTRING(y) self_putstr((char *)y)
#endif
int self_printf_test(void);
int self_outc(char c,unsigned char w);
int self_outs(___va_list_ p);
int self_out_num(int m,int base,unsigned char c,unsigned char w);
int ___va_printf(const char* fmt,char* p);
int self_printf( const char* format,...);
int self_printf_test(void);
int self_putchar(char c);
int self_putstr(char * s);
#endif



