#ifndef SELF_SIGNAL_PROCESSING_H
#define SELF_SIGNAL_PROCESSING_H
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdint.h>

#define		SIG_SHORT_LEN_MAX			8
#define		SIG_DATA_LEN_MAX			64
#define		SIG_BUF_MAX					8
/*  消息类型*/
typedef enum
{
	SIG_CLASSIFY_A = 2,
	SIG_CLASSIFY_B = 3,
	SIG_CLASSIFY_C = 4,
	SIG_CLASSIFY_D = 5,
}MODULE_ID;

/*  消息类型分类*/
typedef enum
{
	MSG_NULL,
	MSG_A_0 = (SIG_CLASSIFY_A << 8),
	MSG_A_1,
	MSG_A_2,
	MSG_A_3,

	MSG_B_0 = (SIG_CLASSIFY_B << 8),
	MSG_B_1,
	MSG_B_2,
	MSG_B_3,

	MSG_C_0 = (SIG_CLASSIFY_C << 8),
	MSG_C_1,
	MSG_C_2,
	MSG_C_3,

	MSG_D_0 = (SIG_CLASSIFY_D << 8),
	MSG_D_1,
	MSG_D_2,
	MSG_D_3,
}MSG_ID_T;

/*  消息结构体*/
typedef struct
{
	MODULE_ID	rcv_id;
	MSG_ID_T	sig_no;
	uint16_t	sig_size;
	void		*p_mess;
	uint8_t		p_data[SIG_SHORT_LEN_MAX];
}SignalT;

/*  消息队列结构体*/
typedef struct{
	uint8_t	sig_rptr;
	uint8_t sig_wptr;
	SignalT sigQue[SIG_BUF_MAX];
}MsgQueT;


#ifdef __cplusplus
}
#endif
#endif