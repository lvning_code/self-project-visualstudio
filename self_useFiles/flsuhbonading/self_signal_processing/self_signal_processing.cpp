#include "self_signal_processing.h"
#include "string.h"
#define FALSE 0;
#define TRUE 1;


MsgQueT m_sig_list;

/* */
void sig_lock(void)
{
	/* Enable global interrupt mask */
	//CyGlobalIntEnable;

}

/* */
void sig_unlock(void)
{
	/* Disable global interrupt mask */
	//CyGlobalIntDisable;

}

/*
* SignalT 信息初始化
*/
void signal_init(SignalT *sig)
{
	sig->rcv_id = (MODULE_ID)0;
	sig->sig_no = (MSG_ID_T)0;
	sig->sig_size = 0;
	sig->p_mess = NULL;
}

/*  消息队列初始化*/
void Sig_Init(void)
{
	m_sig_list.sig_rptr = m_sig_list.sig_wptr = 0;
	memset(m_sig_list.sigQue, 0, SIG_BUF_MAX*sizeof(SignalT));
}

/*
	* 释放内存
*/
void sys_free(void *pv)
{
	if (pv != NULL)
	{
		;
	}
}

/*
	* 申请内存
*/
void* sig_sys_malloc(uint8_t ss)
{
	return NULL;
}


/*
	* 释放消息队列
*/
void Sig_Destroy(SignalT *signal)
{
	sys_free(signal->p_mess);
	signal_init(signal);
}

/* 发送消息 */
uint8_t Sig_Send(SignalT *sig)
{
	//循环队列，从头开始
	if (m_sig_list.sig_wptr >= SIG_BUF_MAX){
		m_sig_list.sig_wptr = 0;
	}

	//队列加满装填
	if (m_sig_list.sig_wptr == m_sig_list.sig_rptr){			
		sig_unlock();
		return FALSE;
	}
	memcpy(&m_sig_list.sigQue[m_sig_list.sig_wptr], sig, sizeof(SignalT));
	m_sig_list.sig_wptr++;

	return TRUE;
}


/* 获取消息 */
uint8_t Sig_Receive(SignalT *sig)
{
	if (m_sig_list.sig_rptr >= SIG_BUF_MAX){
		m_sig_list.sig_rptr = 0;
	}

	if (m_sig_list.sig_rptr != m_sig_list.sig_wptr)
	{
		memcpy(sig, &m_sig_list.sigQue[m_sig_list.sig_rptr], sizeof(SignalT));
		if (sig->sig_size <= SIG_SHORT_LEN_MAX){
			sig->p_mess = sig->p_data;
		}
		m_sig_list.sig_rptr++;
	}
	return FALSE;
}

/*
	* 消息加入队列
*/
void sig_post_message(uint16_t msg_id, uint8_t *p_data, uint8_t data_len)
{
	SignalT sig;
	signal_init(&sig);

	sig.rcv_id = (MODULE_ID)((msg_id >> 8) & 0xff);
	sig.sig_no = (MSG_ID_T)msg_id;
	if (data_len)
	{
		if (data_len > SIG_SHORT_LEN_MAX){
			sig.p_mess = sig_sys_malloc(data_len);
		}	
		else{
			sig.p_mess = sig.p_data;
		}
		memcpy(sig.p_mess, p_data, data_len);
		sig.sig_size = data_len;
	}
	Sig_Send(&sig);
}

/*
	* 消息处理
*/
void sig_dispatch_msg(SignalT *sig)
{
	switch (sig->rcv_id)
	{
	case SIG_CLASSIFY_A:
		switch (sig->sig_no)
		{
		case MSG_A_0:
			break;
		default:
			break;
		}
			break;
	case SIG_CLASSIFY_B:
		break;
	case SIG_CLASSIFY_C:
		break;
	case SIG_CLASSIFY_D:
		break;
	default:
		break;
	}
}


//demo 用法
void sig_handle(void)
{
	SignalT sig;
	if (Sig_Receive(&sig))
	{
		sig_dispatch_msg(&sig);
		Sig_Destroy(&sig);	/*	释放内存消息	*/
	}
}












