#ifndef _STATE_H 
#define _STATE_H 

#include <stddef.h>
#include <stdio.h>
#ifdef _cplusplus
extren "C"
{
#endif

	typedef struct State
	{
		const struct State *(* const stop)(const struct State *pThis);
		const struct State *(* const playOrPause)(const struct State *pThis);
	}State;

	static const State *pCurrentState;
	

	void initialize();
	void onStop();
	void onPlayOrPause();

	const State *ignore(const State *pThis);
	const State *startPlay(const State *pThis);
	const State *stopPlay(const State *pThis);
	const State *pausePlay(const State *pThis);
	const State *resumePlay(const State *pThis);

	static const State IDlE = { ignore, startPlay};
	static const State PLAY = {stopPlay , pausePlay};
	static const State PAUSE = {stopPlay , resumePlay};


#ifdef _cplusplus
}
#endif
#endif