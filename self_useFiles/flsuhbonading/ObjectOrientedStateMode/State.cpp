#include "State.h"
#include <stdio.h>
static unsigned int count = 0;
void initialize()
{
	pCurrentState = &IDlE;
}

void onStop()
{
	pCurrentState = pCurrentState->stop(pCurrentState);
}

void onPlayOrPause()
{
	pCurrentState = pCurrentState->playOrPause(pCurrentState);
}

static const State *ignore(const State *pThis)
{
	printf("[%d] %s\n",++count,"ignore");
	return pCurrentState;
}

static const State *startPlay(const State *pThis)
{
	printf("[%d] %s\n", ++count, "startPlay");
	return &PLAY;
}

static const State *stopPlay(const State *pThis)
{
	printf("[%d] %s\n", ++count, "stopPlay");
	return &IDlE;
}

static const State *pausePlay(const State *pThis)
{
	printf("[%d] %s\n", ++count, "pausePlay");
	return &PAUSE;
}

static const State *resumePlay(const State *pThis)
{
	printf("[%d] %s\n", ++count, "resumePlay");
	return &PLAY;
}