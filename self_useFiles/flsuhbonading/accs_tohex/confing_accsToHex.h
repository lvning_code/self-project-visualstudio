/**************************************************************************//**
 * @file	IIC_confing.h 	
 * @brief For IIC communication
 * @author ning lv
 * @version 0.01
 ******************************************************************************
 * @section License
 ******************************************************************************
 *
 *****************************************************************************/
 
#ifndef _CONFINT_ACCS_TO_HeX_H_
#define _CONFINT_ACCS_TO_HeX_H_

#ifdef __cplusplus
	extern "C" {
#endif
#include <stdint.h>
#define _NULL 0

/*
*** Redefines the exact width signed integer type ...*/
//typedef unsigned          	char uint8;
//typedef unsigned           	int uint32;
//typedef   signed          char uint8_t;

/*
*** Define variables and structures ... */
typedef struct {
	uint8_t Tohex_size;		
	uint8_t Four_Sign;
}accsHex_Struct;

#define Log_Debug   printf
/*
***The global variable ... */
accsHex_Struct AccsHex_Lib;

// point_library
typedef void(*Point_accsHex_Lib)(uint8_t Tohex_size, uint8_t Four_Sign);
//Point_accsHex_Lib p_accsHex_Lib1 = _NULL;


#ifdef __cplusplus
	}
#endif
#endif /*_CONFINT_ACCS_TO_HeX_H_*/





