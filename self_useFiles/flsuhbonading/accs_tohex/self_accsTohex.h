
#include <stdint.h>
#ifndef _SELF_ACCSTOHEX_H_
#define _SELF_ACCSTOHEX_H_

void self_accsTohex_main(void);
uint16_t  ascsToHexs(char *ascs, uint8_t * hexs, uint16_t length);
void Init_AccsHex_Lib(uint8_t Tohex_size, uint8_t Four_Sign);
void self_uartCharText(void);
uint8_t HexToAscs(uint8_t *hex, uint8_t * ascii);
#endif
