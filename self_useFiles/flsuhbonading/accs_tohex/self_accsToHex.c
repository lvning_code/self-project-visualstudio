#include <stdio.h>
#include "string.h"
#include "confing_accsToHex.h"

const uint8_t hexToASCII[16] ={0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,\
    0x41, 0x42, 0x43, 0x44, 0x45,0x46};

/**@brief  Cuint8_t self_Hex(uint8_t *asc)
*
* @param[in]  : *asc 一个字符
* @param[out] : hex 对应的数字值
*/
uint8_t self_Hex(uint8_t *asc)
{
	uint8_t hex;
	if ((*asc >= '0') && (*asc <= '9')){
		hex = *asc - 0x30;
	}
	else if ((*asc >= 'a') && (*asc <= 'f')){
		hex = *asc - 'a' + 0xa;
	}
	else if ((*asc >= 'A') && (*asc <= 'F')){
		hex = *asc - 'A' + 0xa;
	}
	return hex;
}

/**@brief  void self_hexToAscii(uint8_t hex, uint8_t * ascii)
*
* @param[in]  : hex  ， ascii 
*/
uint8_t self_hexToAscii(uint8_t hex, uint8_t * ascii)
{
	uint8_t tmp = 0;
	if (!strlen(ascii)){ return 0 ; };
	tmp = (hex >> 4) & 0x0f;
	*ascii = hexToASCII[tmp];
	tmp = hex & 0x0f;
	*(++ascii) = hexToASCII[tmp];
	return 1;
}


/**@brief  void self_hexToAscii(uint8_t hex, uint8_t * ascii)
*
* @param[in]  : hex  ， ascii
*/
uint8_t HexToAscs(uint8_t *hex, uint8_t * ascii)
{
	uint8_t i = 0;
	uint8_t lenhx = strlen(hex);
	if (!lenhx){ return 0; };
	for (i = 0; i < lenhx; i++){
		self_hexToAscii(hex, ascii);
	}
		return 1;
}


void Init_AccsHex_Lib(uint8_t Tohex_size, uint8_t Four_Sign)
{
	AccsHex_Lib.Four_Sign = Four_Sign;
	AccsHex_Lib.Tohex_size = Tohex_size;
}


/**@brief  uint8_t charsToHex(char *asc)
* 
* @param[in]  : asc 字符串首地址
* @param[out] : hex 对应的数字值
* @param[notes] : 可以将两个个字符转换成一个16进制数字
*/
uint8_t charsToHex(char *asc)
{
	uint8_t hex = 0;
	uint8_t hex_tmp = 0;
	uint8_t *pasc = (uint8_t*) asc;

	hex = self_Hex(&pasc[0]);
	hex = hex << 4;
	hex = hex + self_Hex(&pasc[1]);
	
	if (4 == AccsHex_Lib.Tohex_size)
	{
		hex = self_Hex(&hex);
		hex = hex << 4;
		hex_tmp = self_Hex(&pasc[2]);
		hex_tmp = hex_tmp << 4;
		hex_tmp = hex_tmp + self_Hex(&pasc[3]);
		hex_tmp = self_Hex(&hex_tmp);

		hex += hex_tmp;
	}
	if (hex > 0x10)
		Log_Debug("0x%x ", hex);
	else
		Log_Debug("0x0%x ", hex);
	return hex;
}

uint16_t  ascsToHexs(char *ascs, uint8_t * hexs, uint16_t length)
{
	uint16_t j = 0 , i = 0;
	for (i = 0; i<length; i += AccsHex_Lib.Tohex_size){
		hexs[j++] = charsToHex(ascs + i);
	}
	return j;
}

void uart_putChar(uint8_t ch)
{
		Log_Debug("%c",ch);
}

void uart_sendData(uint8_t *txBuff, uint8_t txSize)
{
  while(txSize--)
      uart_putChar(*txBuff++);
  return ;
}

void uart_putHex(uint8_t data)
{
    uint8_t tmp;
    tmp = (data>>4) & 0x0f;
    uart_putChar(hexToASCII[tmp]);
    tmp = data & 0x0f;
    uart_putChar(hexToASCII[tmp]);
    uart_putChar(' ');
}



void self_hexPackToStr(uint8_t *xpck , uint8_t *str)
{
	
}
void self_uartCharText(void)
{
	uint8_t ascii[2] = {0};
	uart_putHex(10);
	self_hexToAscii(9,ascii);
	uart_sendData(ascii,sizeof(ascii));
}

void self_accsTohex_main(void)
{
	uint8_t cmd_data_buf[128] = { 0 };
	uint8_t cmd_data2_buf[128] = { 0 };
	char* data = "383031304s3413738333538383839394541333031303535353543394436433730";

	Init_AccsHex_Lib(4, (uint8_t)strlen(data));
	printf("cnt:%d\r\n", (uint8_t)strlen(data));
	ascsToHexs(data, cmd_data_buf, (uint8_t)strlen(data));
	printf("\r\n\n");

	ascsToHexs((char*)cmd_data_buf, cmd_data2_buf, (uint8_t)strlen(cmd_data_buf));
	printf("\r\n\n");

	self_uartCharText();
	printf("\r\n\n");
}


