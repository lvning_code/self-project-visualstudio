
#include "stdio.h"
/**@brief  CRC_16(unsigned short CRC_acc, unsigned char *CRC_input, unsigned int len) 
 *
 * @param[in] CRC_acc us 校验结果 ,CRC_input 校验数据，数据长度
 * @example input: 0x63           output:0xBD35
 *					input: 0x8C           output:0xB1F4
 *					input: 0x7D           output:0x4ECA
 *					input: 0xAA,0xBB,0xCC               output:0x6CF6
 *          input: 0x00,0x00,0xAA,0xBB,0xCC     output:0xB166
 */
unsigned short CRC_16(unsigned short CRC_acc, unsigned char *CRC_input, unsigned int len)
{
	unsigned char i,k = 0; 
	#define POLY  0x1021
	while (len--)
	{
		CRC_acc = CRC_acc ^ (CRC_input[k++] << 8);
		for (i = 0; i < 8; i++)
		{
			if ((CRC_acc & 0x8000) == 0x8000)
			{
				CRC_acc = CRC_acc << 1;
				CRC_acc ^= POLY;
			}
			else
			{
				CRC_acc = CRC_acc << 1;
			}
		}
	}
	return CRC_acc;
}
