

目录

# ClassicPractice							/* 练习c文件夹 */
	* rank										/* 判定输入的两行字符串是否相等 */
	* FilsPractice								/* 文件操作 */
	* RotationString							/*判断一个字符串是否为另外一个字符串旋转之后的字符串*/
	* stringFunction							/*A字符串 {s,f,f,e,f,1,4,e,2} 和 字符串 { f,f,e,1,2,4,d,i,w} 存在数字字符映射关系*/
	* SummationOperation						/*项数列的第一项为n，以后各为前一项的平方根，求数列的前m项的和*/
	* //TheLargestInteger						/**/
	* threeLetters								/*一段字符串，输出第一次出现三次的字母（区分大小写）*/
	* maixmum									/*设有n个正整数，将他们连接成一排，组成一个最大的多位整数*/
	* //maxNumber								/*存在问题*/	
	* goodLuck 									/*大于0且小于等于n的幸运数有多少个*/
	* longestNumString							/*求多个字符串中的最大长度的数字字符串，并将该字符串的长度返回*/
	* LetterJudgmentWeek						/*输入字母判定星期几*/
	* //gather									/* 动态数组接收 存在问题待完善*/
	* IntegerAddition							/*请设计一个算法能够完成两个用字符串存储的整数进行相加操作，对非法的输入则返回error*/
	* countDay									/*计算一年天数*/
	* daffodil									/*输出水仙花*/
	* CharacterInversion						/*一只袋鼠要从河这边跳到河对岸*/


# PointerLearningAndPractice				/*指针学习理解加练习*/
	* arrayPearAndPearArray					/*数组与指针联系*/

# studentManagementSystem					/*学生操作系统*/
	*

# ObjectOrientedStateMode
	*
# flsuhbonading								/*嵌入式 开发底层 */				
	* self_i2c								/*嵌入式 模拟I2C */
	*self_printf							/*嵌入式 重写printf */		
	*accs_tohex								/*进制转换*/
	*self_crc								/*crc 校验*/