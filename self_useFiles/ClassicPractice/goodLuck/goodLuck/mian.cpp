﻿///在日常生活中我们最常用的是十进制数，而在计算机中，二进制数也很常用。现在对于一个数字x，小明同学定义出了两个函数f(x)和g(x)。
//f(x)表示把x这个数用十进制写出后各个数位上的数字之和。如f(123)=1+2+3=6。 g(x)表示把x这个数用二进制写出后各个数位上的数字之和。
//如123的二进制表示为1111011，那么，g(123)=1+1+1+1+0+1+1=6。 小明同学发现对于一些正整数x满足f(x)=g(x)，
//他把这种数称为幸运数，现在他想知道，大于0且小于等于n的幸运数有多少个？
#include <stdio.h>
//f(123) = 1 + 2 + 3 = 6   
int f(int i)
{
	int result = 0;
	while (i)
	{
		result += i % 10;
		i /= 10;
	}
	return result;
}
//g(123) = 1 + 1 + 1 +1 + 0 + 1   
int g(int x)
{
	int result = 0;
	while (x)
	{
		result++;
		x &= (x - 1);//同为1，则为1
	}
	return result;
}

int main(void)
{
	int number = 0;
	int sumNumber = 0;
	scanf("%d", &number);
	for (int i = 1; i <= number; i++)
	{
		if (f(i) == g(i))
		{
			sumNumber++;
		}
	}
	printf("%d\n", sumNumber);
	return 0;
}