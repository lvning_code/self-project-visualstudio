﻿// CharacterInversion.cpp : 定义控制台应用程序的入口点。
//
//**********************/
/*
一只袋鼠要从河这边跳到河对岸，河很宽，但是河中间打了很多桩子，每隔一米就有一个，每个桩子上都有一个弹簧，袋鼠跳到弹簧上就可以跳的更远。每个弹簧力量不同，
用一个数字代表它的力量，如果弹簧力量为5，就代表袋鼠下一跳最多能够跳5米，如果为0，就会陷进去无法继续跳跃。河流一共N米宽，
袋鼠初始位置就在第一个弹簧上面，要跳到最后一个弹簧之后就算过河了，给定每个弹簧的力量，求袋鼠最少需要多少跳能够到达对岸。如果无法到达输出-1*/
#include "stdafx.h"
#include<iostream>
#include<stack>
#include<string>
#include<vector>
using namespace std;

int main()
{
	if (1)
	{
		int leng = 0, step = 0;
		int count = 0;
		int i = 0;

		cin >> leng;
		vector<int> springForce(leng);
		for (int i = 0; i < leng; i++)
		{
			cin >> springForce[i];
		}
		
		while (i < leng)
		{
			if (0 == springForce[i])
			{
				cout << -1;
				break;
			}
			i += springForce[i];
			count++;
			//cout << i << endl;
		}
		if (leng <= i)
		{
			cout << count << endl;
		}
	}
	return 0;
}
