﻿#include<stdio.h>
void countDay();
int main()
{
	countDay();
	return 0;
}
//计算天数
void countDay()
{
	int day = 0, 
	    month = 0, 
		year = 0, 
		sum = 0, 
		leapYear = 0;

	printf("\nPlease enter year, month, day. Format: year, month, day\n :");
	scanf("%d,%d,%d", &year, &month, &day);
	switch (month)
	{
		case 1: sum = 0; break;
		case 2: sum = 31; break;
		case 3: sum = 59; break;
		case 4: sum = 90; break;
		case 5: sum = 120; break;
		case 6: sum = 151; break;
		case 7: sum = 181; break;
		case 8: sum = 212; break;
		case 9: sum = 243; break;
		case 10: sum = 273; break;
		case 11: sum = 304; break;
		case 12: sum = 334; break;
		default:printf("data error"); break;
	}
	if (31 == day)
	{
		if (1 == month || 3 == month || 5 == month || 7 == month || 8 == month || 10 == month || 12 == month)
		{
			sum = sum + day;
		}
	}
	else if (30 == day )
	{
		if (4 == month || 6 == month || 9 == month || 11 == month)
		{
			sum = sum + day;
		}
	}
	else if (29 >= day)
	{		
		if (0 == year % 400 || (0 == year % 4 && 0 != year % 100))
		{
			leapYear = 1;
		}
		else
		{
			leapYear = 0;
			sum = sum + day;
		}
		
		if (1 == leapYear && month > 2)
		{
			sum = sum + day + 1;
		}
		else if (1 == leapYear && month < 2)
		{
			sum = sum + day;
		}
		else if (1 == leapYear && month == 2)
		{
			if (28 == day)
			{
				printf("data error");
			}
			else
			{
				sum = sum + day;
			}
		}
	}
	else 
	{
		printf("data error"); 
		return;
	}
	printf("This is the %d day of the year。", sum);
	printf("\n");
}