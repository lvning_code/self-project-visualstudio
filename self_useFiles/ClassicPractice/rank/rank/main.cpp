﻿#include<iostream>
#include<string.h>
using namespace std;
//********************************************************************************************//
//考拉有n个字符串字符串，任意两个字符串长度都是不同的。考拉最近学习到有两种字符串的排序方法：
//1.根据字符串的字典序排序。例如：
//"car" < "carriage" < "cats" < "doggies < "koala"
//2.根据字符串的长度排序。例如：
//"car" < "cats" < "koala" < "doggies" < "carriage"
//考拉想知道自己的这些字符串排列顺序是否满足这两种排序方法，考拉要忙着吃树叶，所以需要你来帮忙验证。
//输入描述 :
//输入第一行为字符串个数n(n ≤ 100)
//接下来的n行, 每行一个字符串, 字符串长度均小于100，均由小写字母组成
//********************************************************************************************//
//重置strrev
char * strrevFunction(char *str)
{
	if (NULL == str)
	{
		return NULL;
	}

	char *s_right = str;
	char *s_left = str;
	char _str;
	for (s_right; *s_right != '\0'; s_right++)
	{
		;
	}	

	s_right--;//del'\0'
	while (s_left < s_right)
	{
		_str = *s_left;
		*s_left = *s_right;
		*s_right = _str;

		s_left++;
		s_right--;
	} //while (s_left = s_right);
	return str;
}
//重置strcpy
char * strcpyFunction(char *str1 , const char *str2)
{
	if ((NULL == str1) || (NULL == str2))
	{
		return NULL;
	}
	if (str1 == str2)
	{
		return str1;
	}

	char *str1s = str1;
	while (*str2 != '\0')
	{
		*str1s = *str2;
		str1s++;
		str2++;
	}
	return str1;
}
//重置strcat 
char * strcatFunction(char *str1 ,const char *str2)
{
	if ((NULL == str1) && (NULL == str2))
	{
		return NULL;
	}

	char *str1s = str1;
	for (str1s; *str1s != '\0'; str1s++)
	{
		;
	}
	while (*str2 != '\0')
	{
		*str1s = *str2;
		str1s ++;
		str2 ++;
	}
	return str1;
}
//重置strlwr 字符串小写的转换,
char * strlwrFunction(char * strlwr)
{
	if (NULL == strlwr)
	{
		return NULL;
	}
	char *str;
	str = strlwr;
	while (*str != '\0')
	{
		if (*str >= 'A' && *strlwr <= 'Z')
		{
			*str += ('a' - 'A');
		}
		str++;//地址下移
	}
	return strlwr;
}
//重置strupr 字符串大写的转换
char* struprFunction(char * strupr)
{
	if (NULL == strupr )
	{
		return NULL;
	}
	char *str;
	str = strupr;
	while (*str != '\0')
	{
		if (*str >= 'a' && *str <= 'z')
		{
			*str += ('A' - 'a');
		}
		str++;
	}
	return strupr;
}
//重置tolower,字母小写转换
int tolowerFunction(int tolower)
{
	if ((tolower >= 'A') && (tolower <= 'Z'))
	{
		return tolower + ('a' - 'A');//差值-\+32
	}
	else
	{
		return tolower;
	}
	
}
//重置toupper，字母大写转换
int toupperFunction(int toupper)
{
	if ((toupper >= 'a') && (toupper <= 'z'))
	{
		return toupper + ('A' - 'a');
	}
	else
	{
		return toupper;
	}
}
//重置strncmp,区分大小
int strncmpFunction(const char *str1 , const char *str2 , int size_n)
{
	int result = 0;
	if (!size_n)
	{
		return -2;
	}
	while (size_n && (*str1 != '\0') && ((unsigned char)*str1 == (unsigned char)*str2))
	{
		size_n--;
		str1++;
		str2++;
	}
	result = (unsigned char)*str1 - (unsigned char)*str2;
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	else
	{
		result = 0;
	}		
	return result;
}
//重置strnicmp，
int strnicmpFunction(const char *str1, const char *str2, int size_n)
{
	int result = 0;
	if (!size_n)
	{
		return -2;
	}
	while (size_n && (*str1 != '\0') && (tolowerFunction(*str1) == tolowerFunction(*str2)))
	{
		size_n--;
		str1++;
		str2++;
	}
	result = tolowerFunction(*str1) - tolowerFunction(*str2);
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	else
	{
		result = 0;
	}
	return result;
}
//重置stricmp
int stricmpFunction(const char *str1, const char *str2)
{
	int result = 0;
	if ((str1 == NULL) || (str2 == NULL))
	{
		cout << "null" << endl;
		return -2;
	}
	unsigned char *str1s = (unsigned char*)(str1);
	unsigned char *str2s = (unsigned char*)(str2);
	while (!(result = toupperFunction(*str1s) - toupperFunction(*str2s)) && (*str1 != '\0'))
	{
		str1s++;
		str2s++;
	}
	//char tmp = result;
	//cout << "结果" << tmp << endl;
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	else
	{
		result = 0;
	}
	return result;
}
//重置strlen
int strlenFunction(const char* str)
{
	int count = 0;
	if (str == NULL)
	{
		return -2;
	}
	while (*str != '\0')
	{
		count ++;
		str ++;
	}
	return count;
}
//重置strcmp 
int strcmpFunction(const char *str1 ,const char *str2)
{
		int result = 0;
		if ((str1 == NULL) || (str2 == NULL))
		{
			cout << "null" << endl;
			return -2;
		}
		//字符串的ASCII没有负值,
		char *str1s = (char*)(str1);
		char *str2s = (char*)(str2);
		while (!(result = *str1s - *str2s) && (*str1 != '\0'))
		{
			str1++;
			str2++;
		}
		if (result < 0)
		{
			result = -1;
		}
		else if (result > 0)
		{
			result = 1;
		}
		else
		{
			result = 0;
		}
		return result;
}

int main()
{
	if (1)
	{
		char str1[20] = "hello";
		char str2[] = " world";
		strcatFunction(str1, str2);
		strrevFunction(str1);
		cout << str1 << endl;


		char str[100];
		cin >> str;
		char *str3 = str;
		char *str4 = strlwrFunction(str);
		cout << str3 << endl;
	}
	int sumStr = 0, i = 0;
	bool isLexico = true;
	bool isLength = true;
	char str[100][100];
	cin >> sumStr;
	for (int i = 0; i < sumStr; i++)
	{
		cin >> str[i];		
	}
	for (int i = 1; i < sumStr; i++)
	{
		if (stricmpFunction(str[i - 1], str[i])>0)
		{
			isLexico = false;
			break;
		}
	}
	for (int i = 1; i < sumStr; i++)
	{
		if (strlenFunction(str[i - 1]) > strlen(str[i]))
		{
			isLength = false;
			break;
		}
	}
	if (isLexico&&isLength)
	{
		cout << "both" << endl;
	}
	else if (isLexico)
	{
		cout << "lexicographically" << endl;
	}
	else if (isLength)
	{
		cout << "lengths" << endl;
	}
	else
	{
		cout << "none" << endl;
	}
	return 0;
}

//bool lexicographically(const char *pOne, const char *pTwo)
//{
//	if (strlen(pTwo) < strlen(pOne))
//		{
//			return false;
//		}	
//	return true;
//}
//
//按照字符串大小排序
//bool lengths( char *pOne, char *pTwo )
//{
//	if (strcmpFunction(pOne, pTwo)>0)
//			{
//				return false;
//			}
//		return true;
//}
	/*for (int i = 0; i < n; i++)
	{
		cout << str[i] << endl;
	}*/
	/*for (int i = 1; i < sumStr; i++)
	{
		if (!lexicographically(str[i - 1], str[i]))
		{
			isLexico = false; 
		}
			
	}
	for (int i = 1; i < sumStr; i++)
	{
		if (!lengths(str[i - 1], str[i]))
		{
			isLength = false;
		}			
	}*/