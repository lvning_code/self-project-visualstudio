﻿// RotationString.cpp : 定义控制台应用程序的入口点。
//判断一个字符串是否为另外一个字符串旋转之后的字符串。 
//例如：
//给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd 和 s2 = ACBD，返回0
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC

#include "stdafx.h"
#include<stdlib.h>
#include<string.h>

//字符串长度
int strlenStr(const char *str)
{
	int length = 0;
	if (NULL == str)
	{
		return length;
	}
	int i = 0;
	while (str[i] != '\0')
	{
		length++;
		i++;
	}
	return length;
}
//字符串的拷贝,将str2中的字符串赋值到str1的'\0'后面(包括'\0'),
//复制宽度为size_str的字符串
char * strcpySizeString(char *str1, const char *str2 , int size_str)
{
	if ((NULL == str1) || (NULL == str2))
	{
		return NULL;
	}
	if (str1 == str2)
	{
		return str1;
	}

	char *str1s = str1;
	while (*str1s != '\0')
	{
		str1s++;
	}
	while ((*str2 != '\0')&&(size_str))
	{
		*str1s = *str2;
		str1s++;
		str2++;
		size_str--;
	}
	*str1s = '\0';
	return str1;
}
//char 行的指针，动态内存初始化为'\0'
bool memsetInifialize(char *p, int size)
{
	if (!size)
	{
		return false;
	}
	for (int i = 0; i < size; i++)
	{
		*p = '\0';
		p++;
	}
	return true;
}
//str1和str2是字符串的地址，比较两者的字符串是符是映射关系,是返回true ，不是返回 false
bool isFindChar(const char *str1 , const char *str2)
{
	int str1_length = 0;
	int str2_length = 0;
	if ((NULL != str1)||(NULL != str2))
	{
		str1_length = strlenStr(str1);
		str2_length = strlenStr(str2);
	}
	else
	{
		return false;
	}

	if (str1_length != str2_length)
	{
		return false;
	}
	
	int countTrue = 0;
	char *strcpy_Str1 = (char *)malloc(sizeof(char)*str1_length + 1);
	char *strcpy_Str2 = (char *)malloc(sizeof(char)*str2_length + 1);
	if((NULL != strcpy_Str1) && (NULL != strcpy_Str2))
	{
		memsetInifialize(strcpy_Str1, str1_length + 1);
		memsetInifialize(strcpy_Str2, str2_length + 1);
		strcpySizeString(strcpy_Str1, str1, str1_length);
		strcpySizeString(strcpy_Str2, str2, str1_length);
	}
	else
	{
		return false;
	}

	for (int i = 0; i < str1_length; i++)
	{
		for (int j = 0; j < str2_length; j++)
		{
			if (*(strcpy_Str1 + i) == *(strcpy_Str2 + j))
			{
				countTrue++;
				*(strcpy_Str2 + j) = '\0';
			}
		}
	}
	free(strcpy_Str1);
	free(strcpy_Str2);

	if (countTrue == str2_length)
	{
		return true;
	}
	return false;
}
//判断是否是旋转字符串
int rotationStr(const char *str1, const char *str2)
{
	int numChar = 0;
	if (!isFindChar(str1, str2))
	{
		return numChar;
	}

	int str1_length = strlenStr(str1);
	char *p_Str1 = (char *)malloc(sizeof(char)*str1_length + 1);
	if (NULL != p_Str1)
	{
		memsetInifialize(p_Str1, str1_length + 1);
	}
	else
	{
		return numChar;
	}

	for (int i = 1; i < str1_length; i++)
	{
		//先存储 i个之后的字符，
		strcpySizeString(p_Str1, str1 + i, str1_length - i);
		strcpySizeString(p_Str1, str1, i);
		if (!strcmp(p_Str1 , str2))
		{
			//free(p_Str1);
			return numChar = i;
		}
		else
		{
			//p_Str1 = (char *)malloc(sizeof(char)*str1_length + 1);
			memsetInifialize(p_Str1, str1_length + 1);
		}
	}
	free(p_Str1);
	return numChar;
}

int main()
{
	char str1[100] = "fheoff";
	char str2[100] = "heofff";
	int result = rotationStr(str1, str2);
	if (result)
	{
		printf("旋转:%d个字符\n",result);
	}
	else
	{
		printf("非旋转\n");
	}
	return 0;
}

