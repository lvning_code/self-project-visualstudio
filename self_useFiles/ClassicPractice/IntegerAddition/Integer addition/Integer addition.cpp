// Integer addition.cpp : 定义控制台应用程序的入口点。
//请设计一个算法能够完成两个用字符串存储的整数进行相加操作，对非法的输入则返回error

#include "stdafx.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
//判定是否是数字字符
bool isNumber(const char str)
{
	if (str >= '0'&& '9' >= str)
	{
		return true;
	}
	return false;
}
//判定指针是空
bool isNull(const void *p)
{
	if (NULL == p)
	{
		return true;
	}
	return false;
}
//计算字符长度，并将指针指向字符串的低端
int lenString(const char *&pStr)
{
	int lenStr = 0;
	for (pStr; *pStr != '\0'; pStr++)
	{
		if (isNumber(*pStr))
		{
			lenStr++;
		}
		else
		{
			return lenStr = 0;
		}
	}
	return lenStr;
}
//字符相加处理函数,前提是pdest 全部初始化了；
bool mergeNumberChar(char * const pdest , const int val , const int address)
{
	if (val > '9')
	{
		*(pdest + address - 1) = 1;
		if (*(pdest + address) == 1)
		{
			*(pdest + address) = val - 9;
		}
		else
		{
			*(pdest + address) = val - 10;
		}
	}
	else if (val == '9')
	{
		if (*(pdest + address) == 1)
		{
			*(pdest + address - 1) = 1;
			*(pdest + address) = '0';
		}
	}
	else
	{
		*(pdest + address) += val;
	}
	return true;
}

//数字字符相加处理函数
bool mergeNumberString(char *pdest, const char *pStr1, const char *pStr2)
{
	if (isNull(pdest) || isNull(pStr1) || isNull(pStr2))
	{
		return false;
	}

	int length = 0, tmp = 0;
	int lenStr1 = lenString(pStr1);
	int lenStr2 = lenString(pStr2);

	if (0 == lenStr1 || 0 == lenStr2)
	{
		printf("error");
		return false;
	}
	else
	{
		length = (lenStr1 > lenStr2) ? lenStr1 : lenStr2;
		memset(pdest, 0, length + 1);
	}

	while (length--)
	{
		if ( --lenStr1 >= 0 && --lenStr2 >= 0)
		{
			tmp = *--pStr1 + *--pStr2 - 48;
			mergeNumberChar(pdest, tmp, length);
		}
		else if (lenStr1 >= 0)
		{
			tmp = *--pStr1;
			mergeNumberChar(pdest, tmp, length);
		}
		else if (lenStr2 >= 0)
		{
			tmp = *--pStr2;
			mergeNumberChar(pdest, tmp, length);
		}
	}
	return true;
}

int main()
{
	char str1[200];
	char str2[200];
	char str3[200];
	int i = 0;
	scanf("%s%s", str1, str2);
	if (mergeNumberString(str3, str1, str2))
	{
		printf(str3);
	}
	return 0;
}