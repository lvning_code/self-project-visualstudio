﻿// threeLetters.cpp : 定义控制台应用程序的入口点。
//一段字符串，输出第一次出现三次的字母（区分大小写）

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//定义枚举 字母大小写
enum Letter
{
	NONLETTER = 0,
	STRUPER,
	STRLWR
};
// 判断字母是目标
int isGoalLetters( char str)
{
	if (('A' <= str)&&(str <='Z'))
	{
		return STRUPER;
		
	}
	else if (('a' <= str)&&(str <= 'z'))
	{
		return STRLWR;
	}
	else
	{
		return NONLETTER;
	}
	
}
//找到最先达到出现三次的字母
int findThreeLetters(const char *str)
{
	if (NULL == str)
	{
		return NONLETTER;
	}

	int * arr = (int *)malloc(sizeof(int) * 52);
	if (NULL == arr)
	{
		return NONLETTER;
	}
	memset(arr, 0, sizeof(int) * 52);
	int letter = 0, site = 0;
	int lengths = strlen(str);

	while (lengths)
	{
		lengths--;
		letter = isGoalLetters(*str);
		switch (letter)
		{
		case STRUPER:
			site = *str - 65; arr[site] += 1; str++; break;
		case STRLWR:
			site = *str - 71; arr[site] += 1; str++; break;
		case NONLETTER:
			str++; break;
		default:
			break;
		}
		if (3 == arr[site])
		{
			return (int)(*(str - 1));
		}
	}
	free(arr);
	arr = NULL;
	return NONLETTER;
}

int main()
{
	char str[1000];
	memset(str , 0 , 1000);
	gets(str);
	//str = "Have you ever gone shopping and";

	int result = findThreeLetters(str);
	if (NONLETTER == result)
	{
		printf("%s\n", "no find three the same of letters");
	}
	else
	{
		printf("%c\n", result);
	}	
	return 0;
}