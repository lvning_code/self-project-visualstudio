﻿// maxNumber.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
void stractFunction(char *str1, char *str2)
{
	if ((NULL == str1) || (NULL == str2))
	{
		return;
	}
	else
	{
		while (*str2++ != '\0')
		{
			if (*(str2 - 1) != ' ')
			{
				*str1++ = *(str2 - 1);
			}
		}
		*str1++ = '\0';
		return;
	}
}
int colonSort(char *pNumber, int cnt)
{
	if (NULL == pNumber)
	{
		return 0;
	}
	else
	{
		int lengths = strlen(pNumber);
		char *p_number = (char *)malloc(sizeof(char)*(lengths + 1));
		char *p_str = (char *)malloc(sizeof(char)*(lengths + 1));

		if ((NULL != p_number) && (NULL != p_str))
		{
			memset(p_number, '\0', sizeof(p_number));
			memset(p_str, '\0', sizeof(p_str));
		}
		else
		{
			return 0;
		}

		strcpy(p_number, pNumber);
		int tmp_min = 0;
		while (cnt--)
		{
			for (int i = 1; i < lengths ; i++)
			{
				if (p_number[i - 1] < p_number[i])
				{
					p_number[i - 1] = ' ';
					stractFunction(p_str, p_number);
					strcpy(p_number, p_str);
					lengths--;
					break;
				}
			}
		}
		printf("%s\n", p_str);
		free(p_number);
		free(p_str);
	}
	return 0;
}
int main()
{
	char number[50];
	int cnt = 0;
	scanf("%s%d", number, &cnt);
	//printf("%s\n", number);
	colonSort(number, cnt);
	return 0;
}
