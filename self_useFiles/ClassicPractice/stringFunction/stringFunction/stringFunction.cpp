﻿//// stringFunction.cpp : 定义控制台应用程序的入口点。
//例子：A字符串 {s,f,f,e,f,1,4,e,2} 和 字符串 { f,f,e,1,2,4,d,i,w} 存在数字字符映射关系
//
#include "stdafx.h"
#include<iostream>
using namespace std;

//判断是否是数字字符的函数
bool isNum(char str)
{
	if ((str >= 48) && (str <= 57))
	{
		return true;
	}
	else
	{
		return false;
	}
}
//计算字符串中数字长度，即 数字字符个数
int numLength(char *str)
{
	int count = 0;
	for (int i = 0; str[i] != '\0'; i++)
	{
		if (isNum(str[i]))
		{
			count++;
		}
	}
	if (0 == count)
	{
		return 0;
	}
	return count;
}
//提取数字字符转存为数组元素中
//*str 中提取数字字符，num是数组地址 。
void pureArray(char *str , int *num)
{
	if (NULL == str || NULL == num)
	{
		return ;
	}
	for (int i = 0; str[i] != '\0'; i++)
	{
		if (isNum(str[i]))
		{
			*num = str[i];
			num++;
		}
	}
}
//求数组元素的和函数
//*str 中提取数字字符，num是数组地址 ，len是数组元素长度
int sumNum(char *str , int *num ,int len)
{
	int result = 0;
	if (NULL == str || NULL == num)
	{
		return result;
	}

	pureArray(str, num);
	for (int i = 0; i < len; i++)
	{ 		
		result += num[i];
	}
	return result;
}
//整型数组元素查找函数
//a为数组地址。len为a 数组长度，num为查找数
//若查找到，则返回true，否则返回false 
bool findNum(int *arr , int len , int num)
{
	if ((NULL == arr) || ('\0' == num))
	{
		return false;
	}
	for (int i = 0; i<len; i++)
	{
		if (arr[i] == num)
		{
			return true;
		}
	}
	return false;
}
//函数实现的功能：
//arr_a 数组内的元素是不是都在arr_b 数组上，是则继续进行，否则返回错误
//arr_b 数组内的元素是不是都在arr_a数组上，是返回正确，否则返回错误

//bool isSame(int *arr_a , int *arr_b , int len_a , int len_b)
//{
	//if ((NULL == arr_a) || (NULL == arr_b))
	//{
	//	return false;
	//}
//	int count = 0;
//	for (int i = 0; i < len_a; i++)
//	{
//		for (int j = 0; j < len_b; j++)
//		{
//			if (arr_b[j] != arr_a[i])
//			{
//				count++;
//				break;
//			}
//		}
//	}
//	if (0 != count)
//	{
//		return false;
//	}
//	for (int i = 0; i < len_b; i++)
//	{
//		for (int j = 0; j < len_a; j++)
//		{
//			if (arr_a[j] != arr_b[i])
//			{
//				count++;
//				break;
//			}
//		}
//	}
//	if (0 == count)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//	
//}
bool isSame(int *arr_a, int *arr_b, int len_a, int len_b)
{
	if ((NULL == arr_a) || (NULL == arr_b))
	{
		return false;
	}

	if (0 == len_a && 0 == len_b)
	{
		cout << "error" << ":";
		return false;
	}
	else if (0 != (len_a - len_b))
	{
		return false;
	}

	for (int i = 0; i < len_a; i++)
	{
		if (!findNum(arr_b , len_b , arr_a[i]))
		{
		return false;
		}
	}

	for (int i = 0; i < len_b; i++)
	{
		if (!findNum(arr_a , len_a , arr_b[i]))
		{
		return false;
		}
	}
	return true;
}
int main()
{
	char str1[100];
	char str2[100];
	cout << "请输入字符串A：" << endl;
	cin >> str1;
	cout << "请输入字符串B：" << endl;
	cin >> str2;
	
	int numa_length = numLength(str1);
	int *numA = (int *)malloc(sizeof(int)*numa_length);

	int numb_length = numLength(str2);
	int *numB = (int *)malloc(sizeof(int)*numb_length);
	//第一种方式
	int reuslt_a = sumNum(str1, numA, numa_length);
	int reuslt_b = sumNum(str2, numB, numb_length);

	if (0 == (reuslt_a - reuslt_b)&&(numa_length == numb_length))
	{
		cout << "one:相同" << endl;
	}
	else
	{
		cout << "one:不同" << endl;
	}
	//第二种方式
	bool result = isSame(numA, numB, numa_length, numb_length);

	if (result)
	{
		cout << "two:相同" << endl;
	}
	else
	{
		cout << "two:不同" << endl;
	}

	free(numB);
	free(numA);
	numA = NULL;
	numB = NULL;
	return 0;
}
