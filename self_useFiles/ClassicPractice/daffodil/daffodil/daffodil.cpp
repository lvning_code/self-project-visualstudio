﻿// daffodil.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<iostream>
using namespace std;

void daffodilFunction(int min , int max);
int main()
{
	int m = 0, n = 0;
	cin >> m >> n;
	daffodilFunction(m , n);
	return 0;
}

//输出水仙花
void daffodilFunction(int min , int max)
{
	if ((100 > min > 999) || (100 > max > 999)||(min > max))
	{
		cout << "error: please input 100 - 999 !" << endl;
	}

	int count = 0;
	int units = 0, tens = 0, hundred = 0;
	for (int number = min; number <= max; number++)
	{
		units = number % 10;
		hundred = number / 100;
		if (hundred)
		{
			tens = (number - units - hundred * 100) / 10;
		}
		else
		{
			tens = (number - units) / 10;
		}

		if (number == (units*units*units + tens*tens*tens + hundred*hundred*hundred))
		{
			if (0 == count)
			{
				cout << number;
			}
			else
			{
				cout << " " << number;
			}
			count++;
		}
	}
	if (0 == count)
	{
		cout << "no";
	}
	cout << endl;
}
