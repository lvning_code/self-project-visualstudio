﻿
// longestString.cpp : 定义控制台应用程序的入口点。
//求多个字符串中的最大长度的数字字符串，并将该字符串的长度返回

#include "stdafx.h"
#include"stdio.h"
#include "string.h"
#include "stdlib.h"

//判定malloc 地址是否成功
bool isSuccessMalloc(void*p)
{
	if (NULL == p)
	{
		perror("malloc false");
		return false;
	}
	return true;
}
//是数字字符返回true 不是返回false
bool isNum(const char str)
{
	if ((str >= 48) && (str <= 57))
	{
		return true;
	}
	return false;
}
//返回数组max_num[0]为，字符串中最长long_str的一段数字个数，
//返回数组max_num[1]为，最大数字段的起始位置
int * longestNum(const char *pStr)
{
	bool flat = true;
	int long_str = 0;
	int max_num[2];
	memset(max_num, 0, 2 * 4);

	if (NULL == pStr)
	{
		return max_num;
	}

	for (int i = 0; pStr[i] != '\0'; i++)
	{
		if (isNum(pStr[i]) && (isNum(pStr[i + 1])))
		{
			long_str++;
		}
		else if (isNum(pStr[i]) && (!isNum(pStr[i + 1])))
		{
			long_str++;
			flat = false;
		}
		if ((long_str >= max_num[0]) && (!flat))
		{
			max_num[0] = long_str;
			max_num[1] = i - long_str + 1;
			long_str = 0;
			flat = true;
		}
	}
	return max_num;
}
//获得最大的连续数字字符段，返回p_str 的地址
char * getlongestNumber(char *pStr, int sumStr)
{
	if (NULL == pStr)
	{
		return NULL;
	}

	char *p_str = (char *)malloc(sizeof(char) * sumStr + 1);
	if (!isSuccessMalloc(p_str))
	{
		return NULL;
	}
	for (int i = 0; i < sumStr + 1;i++)
	{
		p_str[i] = '\0';
	}
	for (int i = 0; i < sumStr; i++)
	{
		p_str[i] = pStr[i];
	}

	return p_str;
}
//找到含有最大数字符串的数组，并将它输出
void printLongestString(char **pStr, int sumStr)
{
	if (NULL == pStr || 0 == sumStr)
	{
		printf("%s", "error:string is null address");
		return;
	}

	int *p_arr = (int *)malloc(sizeof(int) * sumStr * 2);
	if (isSuccessMalloc(p_arr))
	{
		memset(p_arr, 0, sizeof(int) * sumStr * 2);
	}
	else
	{
		return;
	}

	int *p_Temp = NULL;
	for (int i = 0; i < sumStr; i++)
	{
		p_Temp = longestNum(*(pStr + i));
		*(p_arr + i * 2) = *p_Temp;
		*(p_arr + i * 2 + 1) = *(p_Temp + 1);
		
	}
	
	int max = 0;
	int num = 0;
	num = p_arr[0];
	for (int j = 2; j < sumStr *2; j += 2)
	{
		if (p_arr[j] >num)
		{
			max = j;
			num = p_arr[j];
		}
	}

	int long_address = max / 2;
	int address = *(p_arr + max + 1);
	printf("The string with the longest is：%s \n", *(pStr + long_address));
	char *tmp = getlongestNumber((*(pStr + long_address) + address), (p_arr[max]));
	printf("The longest string is：%s \n", tmp);
	free(p_arr);
	free(tmp);
	p_arr = NULL;
	tmp = NULL;
}

int main()
{
	char *str[] = { "ja862383", "shz72", "s112331hfw99","sfeiadwqjw32","iuijuioewi232"
					,"1490-l111jkjik"};
	printLongestString(str, 6);
	return 0;
}

