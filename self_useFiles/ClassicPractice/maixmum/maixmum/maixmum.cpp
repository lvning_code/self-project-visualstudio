﻿// maixmum.cpp : 定义控制台应用程序的入口点。
//
//***********************************************************************************************************//
/*设有n个正整数，将他们连接成一排，组成一个最大的多位整数。
如:n=3时，3个整数13,312,343,连成的最大整数为34331213。
如:n=4时,4个整数7,13,4,246连接成的最大整数为7424613。
输入描述:
有多组测试样例，每组测试样例包含两行，第一行为一个整数N（N<=100），第二行包含N个数(每个数不超过1000，空格分开)。*/
//************************************************************************************************************//
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

//冒号排序的用法
int main()
{
	if(0)
	{
		int numData = 0;
		char inputData[100][5] = { };
		char tmpa[10] = { }, tmpb[10] = { }, tmp[10] = { };
		cin >> numData;
		for (int i = 0; i < numData; i++)
		{
			cin >> inputData[i];
		}
		for (int i = 0; i<numData; i++)
		{
			for (int j = i + 1; j<numData; j++)
			{
				strcpy(tmpa, inputData[i]);
				strcat(tmpa, inputData[j]);//合并
				strcpy(tmpb, inputData[j]);
				strcat(tmpb, inputData[i]);
				if (strcmp(tmpa, tmpb) < 0)
				{
					strcpy(tmp, inputData[j]);
					strcpy(inputData[j], inputData[i]);
					strcpy(inputData[i], tmp);
				}
			}
			cout << inputData[i];
		}	
	}
	if (1)
	{
		/*vector<string> tmpa;
		vector<string> tmpb;*/
		string tmpa = "";
		string tmpb = "";
		int numData = 0;
		cin >> numData;
		vector<string>inputData(numData , "");
		for (int i = 0; i < numData; i++)
		{
			cin >> inputData.at(i);
		}
		for (int i = 0; i < numData; i++)
		{
			for (int j = i + 1; j < numData; j++)
			{
				tmpa = (inputData.at(i) + inputData.at(j));
				tmpb = (inputData.at(j) + inputData.at(i));
				if (tmpa < tmpb)
				{
					inputData.at(j).swap(inputData.at(i));
				}
			}
			cout << inputData.at(i);
		}
		cout << endl;
	}
	
	return 0;
}