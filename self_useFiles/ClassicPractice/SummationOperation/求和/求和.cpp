﻿// 求和.cpp : 定义控制台应用程序的入口点。
//

//项数列的第一项为n，以后各为前一项的平方根，求数列的前m项的和。
#include<stdio.h>
#include<math.h>
int main()
{
	double n = 0.0;//
	int m = 0;
	scanf("%lf%d", &n, &m);
	double sum = 0;
	//double turn = (double)n;
	for (int i = 0; i < m; i++)
	{
		sum += n;
		n = sqrt(n);
	}
	printf("%.3lf\n", sum);
	return 0;
}

