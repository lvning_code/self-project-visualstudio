﻿// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

struct student
{
	int num;  //学生学号 
	char name[10];//学生姓名 
	char sex[4];//学生性别 
	char classroom[10];//学生班级 
	double courseA, scoreB, scoreC; //三门课的成绩 
	double sumScore; //记录个人三门课的总成绩 
};

class school
{
public:
	void inputInformation(); //添加学生信息 
	void addPeople();  //增加学生 
	void delInformation();  //删除学生信息 
	void alterInformation();  //修改学生信息 
	void findInformation();  //查询学生信息 
	void showInformation();  //显示学生信息 
	void synthesize();  //综合统计（各课程平均成绩和合格率） 
	void rank();  //总分排序 
	void back();  //返回主界面 
	void lock();   //修改密码 
	void start();  //管理操作 
};


// TODO:  在此处引用程序需要的其他头文件
