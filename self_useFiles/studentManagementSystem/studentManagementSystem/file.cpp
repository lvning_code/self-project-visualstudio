﻿// stdafx.cpp : 只包括标准包含文件的源文件
// studentManagementSystem.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

void school::start()
{
	cout << setw(35) << "1、Add student information and save it." << setw(30) << "2, delete students' information and save it." << "\n";
	cout << setw(34) << "3、Revise student information and save it." << setw(24) << "4、Ask students for information" << "\n";
	cout << setw(27) << "5、Show student information." << setw(53) << "6、Comprehensive statistics (average grade and pass rate of each course)" << "\n";
	cout << setw(23) << "7、SORT." << setw(31) << "8、To increase students'." << "\n";
	cout << setw(23) << "0、EXIT　SYSTEM." << "\n";
	cout << "Please enter action selection:";
	int i;
	cin >> i; 
	if (9 < i || i < 0) //操作控制 
	{
		cout << "输入有误！" << endl;
		return;
	}
	switch (i)
	{
	case 1:school::inputInformation(); break;
	case 2:school::delInformation(); break;
	case 3:school::alterInformation(); break;
	case 4:school::findInformation(); break;
	case 5:school::showInformation(); break;
	case 6:school::synthesize(); break;
	case 7:school::rank(); break;
	case 8:school::addPeople(); break;
	case 0:exit(0); break;
	default:cout << "error: Input is incorrect!\n";
	}
}
