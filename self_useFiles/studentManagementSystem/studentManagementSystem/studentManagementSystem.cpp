﻿// studentManagementSystem.cpp : 定义控制台应用程序的入口点。
//

//#include "stdafx.h"


#include<iostream> 
#include<fstream> 
#include<iomanip>  //使用setw（）函数 
#include<windows.h> //使用system（）函数 
#include<conio.h>  //使用getch（），输入任意字符 
#include <string>  //使用strcpy（）函数 
using namespace std;
void baocun();//保存信息 
int n = 0;     //全局变量,记录学生总人数。 
char mi[] = "1987"; //初始密码 
class student //学生类 
{
public:
	int num;  //学生学号 
	char name[10];//学生姓名 
	char sex[4];//学生性别 
	char classroom[10];//学生班级 
	double a, b, c; //三门课的成绩 
	double sum; //记录个人三门课的总成绩 
}stu[100], paixu[100], wo[100];//声明对象 

class school :public student
{
public:
	//声明函数原型 
	void input(); //添加学生信息 
	void add();  //增加学生 
	void del();  //删除学生信息 
	void alt();  //修改学生信息 
	void find();  //查询学生信息 
	void disp();  //显示学生信息 
	void cal();  //综合统计（各课程平均成绩和合格率） 
	void xu();  //总分排序 
	void back();  //返回主界面 
	void gai();   //修改密码 
	void start();  //管理操作 
};
//主函数 
void main()
{
	school A;
	char a; char c[20]; char m[20];
	system("cls");//清屏 DOC调用 
	system("color 1F"); //颜色 调用doc 
	cout << setw(45) << "您想进入学生管理系统?" << endl;
	cout << "若进入请按Y，若不进入请按N." << endl;
	cin >> a;
	if (a == 'Y' || a == 'y')
	{
		cout << setw(20) << "0、退出." << setw(15) << "1、登陆." << setw(20) << "2、修改密码." << endl;
		cout << "请输入操作选择:";
		int j;
		cin >> j;
		if (j>3 || j<0)   //输入超出范围控制 
		{
			cout << "输入有误！" << endl;
			A.back();
		}
		switch (j)
		{
		case 1:break;
		case 2:A.gai(); break;
		case 0:exit(0); break;
		default:cout << "你的输入有误!\n";
		}
		system("cls");//清屏 doc调用 
		system("color 3F"); //颜色 同样是调用doc 
		cout << "请输入您的账号:"; cin >> c;
		cout << "请输入您的密码:"; cin >> m;
		if (strcmp(m, mi) == 0 && strcmp(c, "admin") == 0)
		{
			A.start();
		}
		else  //账号、密码输入的错误提示 
		{
			if (strcmp(c, "admin") != 0)
				cout << "账号输入有误，请从新输入!" << endl;
			if (strcmp(m, mi) != 0)
				cout << "密码输入有误，请从新输入!" << endl;
			cout << "按任意键返回" << endl;
			getch();
			main();
		}
	}
	else  //退出系统 
	{
		cout << "谢谢您的关顾!" << endl;
		exit(0);
	}
}

void school::start()
{
	system("cls");//清屏 DOC调用 
	system("color 2F"); //颜色 调用doc 
	cout << setw(45) << "欢迎进入学生管理系统!" << endl;
	cout << setw(35) << "1、添加学生信息 ,并保存." << setw(30) << "2、删除学生信息,并保存." << endl;
	cout << setw(34) << "3、修改学生信息,并保存." << setw(24) << "4、查询学生信息." << endl;
	cout << setw(27) << "5、显示学生信息." << setw(53) << "6、综合统计（各课程平均成绩和合格率.）" << endl;
	cout << setw(23) << "7、总分排序." << setw(31) << "8、增加学生." << endl;
	cout << setw(23) << "0、退出系统." << endl;
	cout << "请输入操作选择:";
	int i;
	cin >> i;
	if (i>9 || i<0) //操作控制 
	{
		cout << "输入有误！" << endl;
		school::back();
	}
	switch (i)
	{
	case 1:school::input(); break;
	case 2:school::del(); break;
	case 3:school::alt(); break;
	case 4:school::find(); break;
	case 5:school::disp(); break;
	case 6:school::cal(); break;
	case 7:school::xu(); break;
	case 8:school::add(); break;
	case 0:exit(0); break;
	default:cout << "你的输入有误!\n";
	}
}

void school::input() //添加学生 
{
	system("color 1F"); //颜色 调用doc 
	system("cls");//清屏 

	cout << "请输入要添加的学生个数:(0-100)";
	cin >> n;
	if (n >= 100 || n <= 0)
	{
		cout << "输入有误!" << endl;
		cout << "按任意键返回" << endl;
		getch();
		input();
	}
	else
	{
		cout << "****注意:输入学号为整数!!性别只能为男或女!!输入的成绩为0-100!!****" << endl;
		cout << "**********************************************" << endl;
		for (int i = 0; i<n; i++)
		{
			cout << "请输入第" << i + 1 << "个同学的信息:" << endl;

			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10)
				<< "英语成绩" << setw(10) << "C++成绩" << setw(10) << "数学成绩" << endl;
			cin >> stu[i].num;   cin >> stu[i].name;
			cin >> stu[i].sex;    cin >> stu[i].classroom;
			cin >> stu[i].a;     cin >> stu[i].b;
			cin >> stu[i].c;
			if (stu[i].num <= 0 && stu[i].num<2147483647)
			{
				cout << "学号输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (strcmp(stu[i].sex, "nan") != 0 && strcmp(stu[i].sex, "nv") != 0)
			{
				cout << "性别输入有误，请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].a<0 || stu[i].a>100)
			{
				cout << "英语成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].b<0 || stu[i].b>100)
			{
				cout << "C++成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].c<0 || stu[i].c>100)
			{
				cout << "数学成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			stu[i].sum = stu[i].a + stu[i].b + stu[i].c;
		}
	}
	baocun();
	school::back();
}
void school::add()
{
	system("color 1F"); //颜色 调用doc 
	system("cls");//清屏 
	int m;
	cout << "请输入要增加的学生个数：";
	cin >> m; n += m;
	if (n >= 100 || n <= 0)
	{
		cout << "输入有误!" << endl;
		cout << "按任意键返回" << endl;
		getch();
		school::input();
	}
	else
	{
		cout << "****注意:输入学号为整数!!性别只能为男或女!!输入的成绩为0-100!!****" << endl;
		for (int i = n - m; i<n; i++)
		{
			cout << "请输入第" << i + 1 << "个同学的信息:" << endl;
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10)
				<< "英语成绩" << setw(10) << "C++成绩" << setw(10) << "数学成绩" << endl;
			cin >> stu[i].num;   cin >> stu[i].name;
			cin >> stu[i].sex;    cin >> stu[i].classroom;
			cin >> stu[i].a;     cin >> stu[i].b;
			cin >> stu[i].c; stu[i].sum = stu[i].a + stu[i].b + stu[i].c;
			if (strcmp(stu[i].sex, "nan") != 0 && strcmp(stu[i].sex, "nv") != 0)
			{
				cout << "性别输入有误，请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].a<0 || stu[i].a>100)
			{
				cout << "英语成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].b<0 || stu[i].b>100)
			{
				cout << "C++成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
				getch(); school::input();
			}
			if (stu[i].c<0 || stu[i].c>100)
			{
				cout << "数学成绩输入有误,请重新输入.\n";
				cout << "按任意键返回" << endl;
			}
		}
	}
	baocun();
	cout << "*********增加后的学生信息*******\n";
	school::disp();
}

void school::xu()//  学生总分从高到低排序 
{
	system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	for (int pass = 1; pass <= n; pass++)
	{
		int work = 0;
		for (int i = 0; i<n; i++)
			paixu[i] = stu[i];
		for (int i = 0; i<n - pass; i++)
			if (paixu[i].sum<paixu[i + 1].sum)
			{
				wo[i] = paixu[i]; paixu[i] = paixu[i + 1];
				paixu[i + 1] = wo[i];
				work = 1;
			} if (work)break;

	}
	cout << "###########总分排序后的信息 .#############" << endl;
	cout << "名次" << setw(8) << "班级" << setw(8) << "姓名" << setw(8) << "总成绩" << endl;
	for (int i = 0; i<n; i++)
	{
		cout << i + 1 << setw(10) << paixu[i].classroom << setw(10) << paixu[i].name << setw(10) << paixu[i].sum << endl;
	}
	school::back();
}

void school::disp()//显示同学信息 
{
	system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	cout << "************学生信息浏览************\n";
	cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(12)
		<< "英语成绩" << setw(8) << "C++" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
	for (int i = 0; i<n; i++)
		cout << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
		<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
		<< '\t' << stu[i].sum << endl;
	school::back();
}


void school::find()
{
	system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	int h, j; char na[20], ban[20];
	cout << "&&&&&进入查询系统&&&&&&&&" << endl;
	cout << "1、按学号查找  2、按姓名查找 \n"
		<< "3、按班级收索  0、返回主页面\n";
	cout << "请输入操作选择:";
	int i;
	cin >> i;
	if (i>8 || i<0) //操作控制 
	{
		cout << "输入有误！" << endl;
		school::back();
	}
	switch (i)
	{
	case 1:
	{system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	cout << "输入学生学号:"; cin >> h;
	for (int i = 0; i<n; i++)
		if (stu[i].num == h)
		{
			cout << "********查找的同学的信息********\n";
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(12)
				<< "英语成绩" << setw(8) << "C++" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
			cout << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
				<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
				<< '\t' << stu[i].sum << endl;
			j++;
		}
	if (j == 0)
		cout << "没有你要查找的信息";
	school::back(); break;
	}
	case 2:
	{system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	cout << "输入学生姓名:"; cin >> na;
	for (int i = 0; i<n; i++)
		if (strcmp(stu[i].name, na) == 0)
		{
			cout << "********查找的同学的信息********\n";
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(12)
				<< "英语成绩" << setw(8) << "C++" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
			cout << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
				<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
				<< '\t' << stu[i].sum << endl;
			j++;
		}
	if (j == 0)
		cout << "没有你要查找的信息";
	school::back(); break;
	}
	case 3:
	{system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	cout << "输入学生班级:"; cin >> ban;
	cout << "********查找的同学的信息********\n";
	for (int i = 0; i<n; i++)
		if (strcmp(stu[i].classroom, ban) == 0)
		{
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(12)
				<< "英语成绩" << setw(8) << "C++" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
			cout << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
				<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
				<< '\t' << stu[i].sum << endl;
			j++;
		}
	cout << "在此班级的一共有:" << j << "人!" << endl;
	if (j == 0)
		cout << "没有你要查找的信息";
	school::back(); break;
	}
	case 0:school::back(); break;
	}
}

void school::del()//删除指定学号学生信息 
{
	system("cls");//清屏; //颜色 同样是调用doc 
	system("color 1F"); //颜色 同样是调用doc 
	int a, y = 0;
	char x;
	cout << "请输入要删除的学生学号";
	cin >> a;
	for (int i = 0; i<n; i++)
		if (stu[i].num == a)
		{
			cout << "该生的信息:\n";
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10)
				<< "英语成绩" << setw(10) << "C++成绩" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
			cout << stu[i].num << " " << stu[i].name << '\t' << stu[i].sex
				<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
				<< '\t' << stu[i].sum << endl;
			cout << "是否确认删除？（Y/N）" << endl;
			cin >> x;
			if (x == 'Y' || x == 'y')
			{
				y++;
				for (; i<n; i++)
					stu[i] = stu[i + 1];
				n = n - 1;//每删除一个总是减少一个 
			}
			else
			{
				cout << "退出删除!" << endl;
				school::back();
			}
		}

	if (y == 0)
	{
		cout << "该学生不存在！" << endl;
		school::back();
	}
	else
	{
		baocun();
		cout << "删除后的信息为：" << endl;
		school::disp();
		school::back();
	}
}

void school::alt()//修改指定学号学生  
{
	system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	int xh, y = 0;
	char x;
	cout << "请输入要修改学生的学号：";
	cin >> xh;
	for (int i = 0; i<n; i++)
		if (stu[i].num == xh)
		{
			cout << "该生的信息:\n";
			cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10)
				<< "英语成绩" << setw(10) << "C++成绩" << setw(10) << "数学成绩" << endl;
			cout << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
				<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
				<< '\t' << endl;
			cout << "是否确认修改？（Y/N）" << endl;
			cin >> x;
			if (x == 'Y' || x == 'y')
			{
				y++;
				cout << "****注意:输入学号为整数!!性别只能为男或女!!输入的成绩为0-100!!****" << endl;
				cout << "**********************************************************" << endl;
				cout << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10)
					<< "英语成绩" << setw(10) << "C++成绩" << setw(10) << "数学成绩" << endl;
				cin >> stu[i].num;   cin >> stu[i].name;
				cin >> stu[i].sex;    cin >> stu[i].classroom;
				cin >> stu[i].a;     cin >> stu[i].b;
				cin >> stu[i].c;     stu[i].sum = stu[i].a + stu[i].b + stu[i].c;
			}
			else
			{
				school::back();
			}
		}
	if (y == 0)
	{
		cout << "该学生不存在!";
		school::back();
	}
	else
	{
		baocun();
		cout << "***********修改后的信息为************" << endl;
		school::disp();
	}
}
void school::cal()//综合统计（各课程平均成绩和合格率） 
{
	system("color 1F"); //颜色 同样是调用doc 
	system("cls");//清屏 
	double english = 0, program = 0, math = 0;
	for (int i = 0; i<n; i++)
	{
		english += stu[i].a;
		program += stu[i].b;
		math += stu[i].c;
	}
	cout << "**********各课程平均成绩为**********" << endl;
	cout << "英语平均成绩:" << english / n << endl;
	cout << "C++平均成绩:" << program / n << endl;
	cout << "数学平均成绩:" << math / n << endl;
	double t = 0, y = 0, u = 0;
	for (int i = 0; i<n; i++)
	{
		if (stu[i].a>60)t++;
		if (stu[i].b>60)y++;
		if (stu[i].c>60)u++;
	}
	cout << "**********各课程合格率**********" << endl;
	cout << "英语合格率:" << t / n * 100 << "%" << endl;
	cout << "C++合格率:" << y / n * 100 << "%" << endl;
	cout << "数学合格率:" << u / n * 100 << "%" << endl;
	school::back();
}
void school::gai()//修改密码 
{
	system("cls");//清屏; //颜色 同样是调用doc 
	system("color 2F"); //颜色 同样是调用doc 
	char mi2[20], mi3[20], mi4[20];
	cout << "&&&&&&&欢迎修改密码!&&&&&&&&&" << endl;
	cout << "请输入原密码:"; cin >> mi2;

	if (strcmp(mi2, mi) == 0)
	{
		cout << "请输入新密码:"; cin >> mi3;
		cout << "请再输入新密码:"; cin >> mi4;
	}
	else
	{
		cout << "密码错误，请重新输入." << endl;
		cout << "按任意键返回!" << endl; getch();
		system("cls");//清屏; //颜色 同样是调用doc 
		system("color 2F"); //颜色 同样是调用doc 
		gai();
	}
	if (strcmp(mi3, mi4) == 0)
	{
		cout << "恭喜修改密码成功!" << endl; strcpy(mi, mi3);
		cout << "按任意键返回!" << endl;
		getch(); main();
	}
	else
	{
		cout << "前后密码不同，请从新输入!" << endl;
		cout << "按任意键返回!" << endl;
		getch();
		system("cls");//清屏; //颜色 同样是调用doc 
		system("color 2F"); //颜色 同样是调用doc 
		gai();
	}
}
void baocun()//保存信息 
{
	char ch; school B;
	cout << "是否要保存信息?(Y/N)\n";
	cin >> ch;
	if (ch == 'Y' || ch == 'y')
	{
		char filename[20];
		fstream f; char answer; char s[80];
		cout << "请输入要保存文件的路径:(如:D:\\student.dat)\n";
		cin >> filename;
		f.open(filename, ios::out);
		f << "**************存取的学生信息***********" << endl;
		f << "学号" << setw(7) << "姓名" << setw(7) << "性别" << setw(7) << "班级" << setw(10) << "英语成绩" <<
			setw(10) << "C++成绩" << setw(10) << "数学成绩" << setw(13) << "该同学总分" << endl;
		for (int i = 0; i<n; i++)
			f << stu[i].num << '\t' << stu[i].name << '\t' << stu[i].sex
			<< '\t' << stu[i].classroom << '\t' << stu[i].a << '\t' << stu[i].b << '\t' << stu[i].c
			<< '\t' << stu[i].sum << endl;
		f.close();
		cout << "信息已经保存，要浏览吗?(Y/N)\n";
		cin >> answer;
		if (answer == 'Y' || answer == 'y')
		{
			f.open(filename, ios::in);//重用流打开文件 
			while (!f.eof())
			{
				f.getline(s, 80);
				cout << s << endl;
			}

		}
		f.close();//关闭文本文件 
		B.back();
		system("cls");//清屏; //颜色 同样是调用doc 
		system("color 2F"); //颜色 同样是调用doc 
	}
}
void school::back()//返回 
{
	cout << "按任意键返回!" << endl;
	getch();
	school::start();
}

